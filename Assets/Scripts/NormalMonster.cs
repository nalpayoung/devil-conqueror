using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

[System.Serializable]
public class NormalMonster
{
    public int monsternum;
    public string name;
    public float hp;
    public int hpDigit;
    public float atk;
    public int atkDigit;
    public float initialprice;
    public int initialpriceDigit;
    public float cooltime;
    public int monstertype;
    
    public NormalMonster(int _monsterNum, string _name, float _hp, int _hpDigit, float _atk, int _atkDigit,
        float _initialprice, int _initialpriceDigit)
    {        
        monsternum = _monsterNum;        
        name = _name;        
        hp = _hp;
        hpDigit = _hpDigit;
        atk = _atk;
        atkDigit = _atkDigit;
        initialprice = _initialprice;
        initialpriceDigit = _initialpriceDigit;
        cooltime = 5f + (_monsterNum - 200f) * 0.5f;
        monstertype = 2;
    }
}
