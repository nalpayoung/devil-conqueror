using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class EquipMentManager : MonoBehaviour
{
    public bool isOpend = false;
    UserDataController theUser;
    DataBaseManager theData;
    CurrencyManager theCurrency;
    EquipmentSlotChecker theSlotCheker;

    public TextMeshProUGUI Tm_Title, Tm_Grade, Tm_CurrLv;
    public Image Img_BG, Img_Object;    
    public TextMeshProUGUI Tm_equipEff, Tm_invenEff0, Tm_invenEff1, Tm_invenEff2;
    public TextMeshProUGUI Tm_equipEffValue, Tm_invenEffValue1, Tm_invenEffValue2, Tm_invenEffValue3;
    public TextMeshProUGUI Tm_equipEffNValue, Tm_invenEffNValue1, Tm_invenEffNValue2, Tm_invenEffNValue3;
    public TextMeshProUGUI Tm_Count;
    public TextMeshProUGUI Tm_currPrice;
    public TextMeshProUGUI Tm_equiped;
    public Button Btn_equip;
    public Button Btn_ComBine, Btn_CombineUn;
    public Button Btn_LevelUp, Btn_LevelUpUn;

    public Vector3 initialPos;

    public int currType;
    public int currNo;
    public int i_grade;
    public int i_currLv;
    public int i_count;
    public float s_currEquipEff, s_nextEquipEff;
    public float s_currInvenEff1, s_nextInvenEff1;
    public float s_currInvenEff2, s_nextInvenEff2;
    public float s_currInvenEff3, s_nextInvenEff3;
    public float s_currPrice;
    float[] currPrice = new float[2];

    bool isPerchasable = false;    
    public int armorEquipNo, weaponEquipNo = 0;
    //유저데이터에 인벤토리 효과들도 다 가져왔는지 확인

    float currTime = 0;
    float checkTime = 1.5f;

    private void Start()
    {
        theUser = FindObjectOfType<UserDataController>();
        theData = FindObjectOfType<DataBaseManager>();
        theCurrency = FindObjectOfType<CurrencyManager>();       
        initialPos = transform.position;
    }

    private void FixedUpdate()
    {
        currTime += Time.deltaTime;
        if(currTime >= checkTime)
        {
            UpgradableCheck();
            currTime = 0;
        }
    }

    public void AcitvOn()
    {
        if (currType == 0)
        {
            InsertWeaponAllValue();
        }
        else
        {
            InsertArmorAllValue();
        }
        FillText();
        SetBtnStatement();
        UpgradableCheck();
    }

    public void Close()
    {
        transform.position = initialPos;
    }

    public void InsertWeaponAllValue()
    {       
        s_currEquipEff = GetTypeAndInsertData(theData.WeaponEquipedAtkAddData[i_currLv][currNo.ToString()]);
        s_nextEquipEff = GetTypeAndInsertData(theData.WeaponEquipedAtkAddData[i_currLv + 1][currNo.ToString()]);
        s_currInvenEff1 = GetTypeAndInsertData(theData.WeaponUnequipedAtkAddData[i_currLv][currNo.ToString()]);
        s_nextInvenEff1 = GetTypeAndInsertData(theData.WeaponUnequipedAtkAddData[i_currLv + 1][currNo.ToString()]);
        if (i_grade > 2)
        {
            s_currInvenEff2 = GetTypeAndInsertData(theData.WeaponData[i_currLv]["weaponCritDamage"]);
            s_nextInvenEff2 = GetTypeAndInsertData(theData.WeaponData[i_currLv + 1]["weaponCritDamage"]);
            if(i_grade == 5)
            {
                s_currInvenEff3 = GetTypeAndInsertData(theData.WeaponData[i_currLv]["weaponGoldEarn"]);
                s_nextInvenEff3 = GetTypeAndInsertData(theData.WeaponData[i_currLv + 1]["weaponGoldEarn"]);
            }
            else
            {
                s_currInvenEff3 = 0;
                s_nextInvenEff3 = 0;
            }
        }
        else
        {
            s_currInvenEff2 = 0;
            s_nextInvenEff2 = 0;
            s_currInvenEff3 = 0;
            s_nextInvenEff3 = 0;
        }
        s_currPrice = GetTypeAndInsertData(theData.equipPriceData[i_currLv][currNo.ToString()]);
        if(s_currPrice > 1000)
        {            
            currPrice = theCurrency.Currencying(s_currPrice);
            s_currPrice = 0;
        }
        else
        {
            currPrice[0] = 0;
            currPrice[1] = 0;
        }
        i_count = theUser.weaponCount[currNo];        
    }

    public void InsertArmorAllValue()//
    {        
        s_currEquipEff = GetTypeAndInsertData(theData.ArmorEquipedHpAddData[i_currLv][currNo.ToString()]);
        s_nextEquipEff = GetTypeAndInsertData(theData.ArmorEquipedHpAddData[i_currLv + 1][currNo.ToString()]);
        s_currInvenEff1 = GetTypeAndInsertData(theData.ArmorUnequipedHpAddData[i_currLv][currNo.ToString()]);
        s_nextInvenEff1 = GetTypeAndInsertData(theData.ArmorUnequipedHpAddData[i_currLv + 1][currNo.ToString()]);
        if (i_grade > 2)
        {
            s_currInvenEff2 = GetTypeAndInsertData(theData.ArmorData[i_currLv]["armorManaRecovery"]) *
                Mathf.Pow(10, GetTypeAndInsertData(theData.ArmorData[i_currLv]["armorManaRecoveryLog10"]));
            s_currInvenEff2 = GetTypeAndInsertData(theData.ArmorData[i_currLv + 1]["armorManaRecovery"]) *
                Mathf.Pow(10, GetTypeAndInsertData(theData.ArmorData[i_currLv + 1]["armorManaRecoveryLog10"]));
            if (i_grade == 5)
            {
                s_currInvenEff3 = GetTypeAndInsertData(theData.ArmorData[i_currLv]["armorExp"]);
                s_nextInvenEff3 = GetTypeAndInsertData(theData.ArmorData[i_currLv + 1]["armorExp"]);
            }
            else
            {
                s_currInvenEff3 = 0;
                s_nextInvenEff3 = 0;
            }
        }
        else
        {
            s_currInvenEff2 = 0;
            s_nextInvenEff2 = 0;
            s_currInvenEff3 = 0;
            s_nextInvenEff3 = 0;
        }
        s_currPrice = GetTypeAndInsertData(theData.equipPriceData[i_currLv][currNo.ToString()]);
        if (s_currPrice > 1000)
        {
            currPrice = theCurrency.Currencying(s_currPrice);
        }
        else
        {
            currPrice[0] = 0;
            currPrice[1] = 0;
        }
        i_count = theUser.weaponCount[currNo];        
    }

    public void FillText()
    {
        if (currType == 0)
        {
            Tm_Title.text = "WAND";
            i_grade = (int)theData.WeaponData[currNo]["weaponGrade"];
            i_currLv = theUser.weaponLv[currNo];           
            Img_Object.GetComponent<Image>().sprite = Resources.Load<Sprite>("images/equipment/" + "w" + currNo.ToString());
            Tm_equipEff.text = "ATK+";
            Tm_invenEff0.text = "ATK+";
            Tm_invenEff1.text = "CritDmg+";
            Tm_invenEff2.text = "Gold Earn+";
        }
        else
        {
            Tm_Title.text = "ARMOR";
            i_grade = (int)theData.ArmorData[currNo]["armorGrade"];
            i_currLv = theUser.armorLv[currNo];
            Img_Object.sprite = Resources.Load<Sprite>("images/equipment/" + "a" + currNo.ToString());
            Tm_equipEff.text = "HP+";
            Tm_invenEff0.text = "HP+";
            Tm_invenEff1.text = "Mana Recovery+";
            Tm_invenEff2.text = "EXP Earn+";
        }

        Img_BG.sprite = Resources.Load<Sprite>("images/bg/" + i_grade.ToString());

        if (i_grade == 0)
        {
            Tm_Grade.text = "Common";
            Tm_invenEff1.text = "";
            Tm_invenEff2.text = "";
        }
        else if (i_grade == 1)
        {
            Tm_Grade.text = "Uncommon";
            Tm_invenEff1.text = "";
            Tm_invenEff2.text = "";
        }
        else if (i_grade == 2)
        {
            Tm_Grade.text = "Rare";
            Tm_invenEff2.text = "";
        }
        else if (i_grade == 3)
        {
            Tm_Grade.text = "Epic";
            Tm_invenEff2.text = "";
        }
        else if (i_grade == 4)
        {
            Tm_Grade.text = "Legend";
            Tm_invenEff2.text = "";
        }
        else
        {
            Tm_Grade.text = "Mythic";
        }
        Tm_CurrLv.text = "LV." + i_currLv.ToString();        

        Tm_equipEffValue.text = (s_currEquipEff * 100).ToString() + "%";
        Tm_equipEffNValue.text = (s_nextEquipEff * 100).ToString() + "%";
        Tm_invenEffValue1.text = (s_currInvenEff1 * 100).ToString() + "%";
        Tm_invenEffNValue1.text = (s_nextInvenEff1 * 100).ToString() + "%";
        Tm_invenEffValue2.text = (s_currInvenEff2 * 100).ToString() + "%";
        Tm_invenEffNValue2.text = (s_nextInvenEff2 * 100).ToString() + "%";
        Tm_invenEffValue3.text = (s_currInvenEff3 * 100).ToString() + "%";
        Tm_invenEffNValue3.text = (s_nextInvenEff3 * 100).ToString() + "%";

        if(s_currPrice != 0)
        {
            Tm_currPrice.text = s_currPrice.ToString();
        }
        else
        {
            Tm_currPrice.text = theCurrency.NumberToString(currPrice);
        }       
        Tm_Count.text = i_count.ToString() + "/5";
    }
    
    public void SetBtnStatement()
    {
        if(currType == 0)
        {
            if (!theUser.weaponActivate[currNo])//입수했는가
            {
                Btn_LevelUp.gameObject.SetActive(false);
                Btn_equip.gameObject.SetActive(false);
                Tm_equiped.gameObject.SetActive(false);                
            }

            else
            {
                Btn_LevelUp.gameObject.SetActive(true);              
               
                if(theUser.equipedWeaponNo == currNo)//장착했는가 아닌가
                {
                    Btn_equip.gameObject.SetActive(false);
                    Tm_equiped.gameObject.SetActive(true);
                }
                else
                {
                    Btn_equip.gameObject.SetActive(true);
                    Tm_equiped.gameObject.SetActive(false);
                }

                if(i_count > 5 && currNo!=23)//결합이 가능하냐 아니냐
                {
                    Btn_CombineUn.gameObject.SetActive(false);
                }
                else
                {
                    Btn_CombineUn.gameObject.SetActive(true);
                }               
            }
        }
        else
        {
            if (!theUser.weaponActivate[currNo])//입수했는가
            {
                Btn_LevelUp.gameObject.SetActive(false);
                Btn_equip.gameObject.SetActive(false);
                Tm_equiped.gameObject.SetActive(false);
                Btn_CombineUn.gameObject.SetActive(true);
            }

            else
            {
                Btn_LevelUp.gameObject.SetActive(true);

                if (theUser.equipedWeaponNo == currNo)//장착했는가 아닌가
                {
                    Btn_equip.gameObject.SetActive(false);
                    Tm_equiped.gameObject.SetActive(true);
                }
                else
                {
                    Btn_equip.gameObject.SetActive(true);
                    Tm_equiped.gameObject.SetActive(false);
                }

                if (i_count > 5 && currNo != 23)//결합이 가능하냐 아니냐
                {
                    Btn_CombineUn.gameObject.SetActive(false);
                }
                else
                {
                    Btn_CombineUn.gameObject.SetActive(true);
                }
            }
        }
    }

    public void UpgradableCheck()
    {
        if (Btn_LevelUp.gameObject.activeSelf)
        {
            if (theCurrency.CompareFloat(theUser.currStone, currPrice) == 1 || theCurrency.CompareFloat(theUser.currStone, currPrice) == 2 && i_currLv < 250)
            {
                Btn_LevelUpUn.gameObject.SetActive(false);
            }
            else
            {
                Btn_LevelUpUn.gameObject.SetActive(true);
            }
        }
    }    

    public void FullLevel()
    {
        Btn_LevelUp.gameObject.SetActive(false);
        Btn_equip.gameObject.SetActive(false);
    }


    public void Btn_Right()
    {        
        currNo++;
        if(currNo == 24)
        {
            currNo = 0;
        }
        if (currType == 0)
        {
            InsertWeaponAllValue();
        }
        else
        {
            InsertArmorAllValue();
        }
        FillText();
        SetBtnStatement();
        UpgradableCheck();
    }

    public void Btn_Left()
    {
        currNo--;
        if (currNo == -1)
        {
            currNo = 23;
        }
        if (currType == 0)
        {
            InsertWeaponAllValue();
        }
        else
        {
            InsertArmorAllValue();
        }
        FillText();
        SetBtnStatement();
        UpgradableCheck();
    }    

    public void LevelUp()//유저데이터 변경 필요
    {
        i_currLv++;
        if(i_currLv == 250)
        {
            FullLevel();
        }
        theUser.LevelUpEquipment(currType, currNo);
        theUser.SpendStone(currPrice);
        if (currType == 0)
        {
            InsertWeaponAllValue();
        }
        else
        {
            InsertArmorAllValue();
        }
        FillText();
        SetBtnStatement();
        UpgradableCheck();
    }

    public void Equip()//유저데이터 변경 필요
    {
        theUser.ChangeEquipment(currType, currNo);
        Btn_equip.gameObject.SetActive(false);
        Tm_equiped.gameObject.SetActive(true);
    }
    

    public void CombineEquipment()
    {
        theUser.CombieEquipment(currType, currNo);
        FillText();
        SetBtnStatement();
        UpgradableCheck();
    }

    public float GetTypeAndInsertData(object _object)
    {        
        if (_object.GetType().Name == "Int32")
        {
            return (int)_object;
        }
        else
        {
            return TrunCate((float)_object);
        }
    }

    public float TrunCate(float _float)
    {
        if (_float > 100)
        {
            _float = Mathf.Round(_float);
        }
        else
        {
            return _float;
        }
        return _float;
    }

}
