using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class MonsterSlot : MonoBehaviour
{    
    CurrencyManager theCurrency;
    SlotController theSlot;
    UserDataController theUser;
    DataBaseManager theData;
    SliderController theSlider;

    //레벨업 버튼
    public bool isBtnDowning = false;
    public float levelUPCoolTime = 0.5f;
    public float levelUpWaitSecond = 0;
    

    //UI들
    public GameObject Active, Unactive;        
    public Slider slider;
    public TextMeshProUGUI Txt_name, Txt_level, Txt_atk, Txt_hp, Txt_unactiveCost;
    public TextMeshProUGUI txt_UpgradePrice;
    public Button Btn_Upgrade, Btn_Waiting;
    public Button Btn_Recruit, Btn_RecruitWaiting;

    //몬스터인덱스 0부터 시작
    public int slotIndex = 0;

    //DataBase에서 초기값을 받아오고 값을 갖고 있다가 계산해서 몬스터에게 전해주는 값들
    public int currLevel = 0;
    public string st_name;
    public float[] initialAtk, currAtk, atkAddValue = new float[2];
    public float[] initialHP, currHP, hpAddValue = new float[2];
    public float[] initialPrice, currPrice, priceAddValue = new float[2];//최초값의 0.1f
    public Image modelImg;   
    

    private void Start()
    {
        theUser = FindObjectOfType<UserDataController>();
        theCurrency = FindObjectOfType<CurrencyManager>();
        theSlot = FindObjectOfType<SlotController>();
        theData = FindObjectOfType<DataBaseManager>();
        theSlider = FindObjectOfType<SliderController>();



        SetInitialData();
        FillTexts();

        levelUpWaitSecond = levelUPCoolTime; 
    }

    void Update()
    {
        if (isBtnDowning)
        {
            if(levelUpWaitSecond <= 0)
            {
                LevelUP();                
                if(levelUPCoolTime <= 0.05f)
                {
                    levelUPCoolTime = 0.05f;
                }
                else
                {
                    levelUPCoolTime -= 0.05f;
                }
                levelUpWaitSecond = levelUPCoolTime;
            }
            else
            {
                levelUpWaitSecond -= Time.deltaTime;
            }
        }
    }

    public void PointerDown()
    {
        isBtnDowning = true;
    }

    public void PointerUp()
    {
        isBtnDowning = false;
    }

    void SetInitialData()//UserController에서 초기값 받아오기//슬라이더밸류 어떻게 할건지 결정해야함
    {
        modelImg.sprite = Resources.Load<Sprite>("images/normal/" + (200 + slotIndex).ToString());
        currLevel = theUser.a_normalMonsterSlotLv[slotIndex];
        st_name = theData.NormalMonsterData[slotIndex]["normalMonsterName"].ToString();

        object obj = theData.NormalMonsterData[slotIndex]["normalMonsterAtk"];
        if (obj.GetType().Name == "Int32")
        {
            initialAtk[0] = (int)theData.NormalMonsterData[slotIndex]["normalMonsterAtk"];
        }
        else
        {
            initialAtk[0] = (float)theData.NormalMonsterData[slotIndex]["normalMonsterAtk"];
        }

        obj = theData.NormalMonsterData[slotIndex]["normalMonsterAtkLog10"];
        if (obj.GetType().Name == "Int32")
        {
            initialAtk[1] = (int)theData.NormalMonsterData[slotIndex]["normalMonsterAtkLog10"];
        }
        else
        {
            initialAtk[1] = (float)theData.NormalMonsterData[slotIndex]["normalMonsterAtkLog10"];
        }

        atkAddValue[0] = initialAtk[0];
        atkAddValue[1] = initialAtk[1] - 1;
        CalcurateCurrAtk();

        obj = theData.NormalMonsterData[slotIndex]["normalMonsterHp"];
        if (obj.GetType().Name == "Int32")
        {
            initialHP[0] = (int)theData.NormalMonsterData[slotIndex]["normalMonsterHp"];
        }
        else
        {
            initialHP[0] = (float)theData.NormalMonsterData[slotIndex]["normalMonsterHp"];
        }

        obj = theData.NormalMonsterData[slotIndex]["norMonsterHpLog10"];
        if (obj.GetType().Name == "Int32")
        {
            initialHP[1] = (int)theData.NormalMonsterData[slotIndex]["norMonsterHpLog10"];
        }
        else
        {
            initialHP[1] = (float)theData.NormalMonsterData[slotIndex]["norMonsterHpLog10"];
        }
        
        hpAddValue[0] = initialHP[0];
        hpAddValue[1] = initialHP[1] - 1;
        CalcurateCurrHP();

        obj = theData.NormalMonsterData[slotIndex]["norMonsterPrice"];
        if (obj.GetType().Name == "Int32")
        {
            initialPrice[0] = (int)theData.NormalMonsterData[slotIndex]["norMonsterPrice"];
        }
        else
        {
            initialPrice[0] = (float)theData.NormalMonsterData[slotIndex]["norMonsterPrice"];
        }

        obj = theData.NormalMonsterData[slotIndex]["norMonsterPriceLog10"];
        if (obj.GetType().Name == "Int32")
        {
            initialPrice[1] = (int)theData.NormalMonsterData[slotIndex]["norMonsterPriceLog10"];
        }
        else
        {
            initialPrice[1] = (float)theData.NormalMonsterData[slotIndex]["norMonsterPriceLog10"];
        }

        priceAddValue[0] = initialPrice[0];
        priceAddValue[1] = initialPrice[1] - 1;
        CalcurateCurrPrice();
    }

    public void CalcurateCurrAtk()
    {
        currAtk = theCurrency.Multiple(atkAddValue, currLevel);
        currAtk = theCurrency.Plus(initialAtk, currAtk);
    }

    public void CalcurateCurrHP()
    {
        currHP = theCurrency.Multiple(hpAddValue, currLevel);
        currHP = theCurrency.Plus(initialHP, currHP);
    }

    public void CalcurateCurrPrice()
    {
        currPrice = theCurrency.Multiple(priceAddValue, currLevel);
        currPrice = theCurrency.Plus(initialPrice, currAtk);        
    }

    public void FillTexts()//현재 데이터를 UI에 채우기
    {        
        Txt_name.text = st_name;
        Txt_level.text = "Lv " + currLevel.ToString();
        Txt_atk.text = "Atk " + theCurrency.NumberToString(currAtk);
        Txt_hp.text = "Hp " + theCurrency.NumberToString(currHP);
        Txt_unactiveCost.text = theCurrency.NumberToString(initialPrice);
        
        txt_UpgradePrice.text = theCurrency.NumberToString(currPrice);        
    }

    
    public void LevelUP()//레벨업시 바뀌는 데이터들 정리
    {
        int a = theCurrency.CompareFloat(currPrice, theUser.currGold);

        if(a == 1 || a == 2)
        {
            theUser.SpendGold(currPrice);
            currLevel++;
            theUser.a_normalMonsterSlotLv[slotIndex]++;
            CalcurateCurrAtk();
            CalcurateCurrHP();
            CalcurateCurrPrice();
            FillTexts();
        }

        else
        {
            Debug.Log("골드가 부족하셔셩");
        }
    }    

    public void BuySlot()
    {       
        if (theUser.monsterSlotActiveNumber == 29)//벌어지지 않을일. 버튼이 없기 때문에
        {
            Debug.Log("모든 슬롯 오픈중입니다");
        }
        else
        {
            int a = theCurrency.CompareFloat(currPrice, theUser.currGold);
            if( a == 1 || a == 2)
            {
                theUser.SpendGold(currPrice);
                theSlot.BuyNormalMonsterSlot(slotIndex);
                theSlider.MakeNormalMonsterChecker(slotIndex);
            }
            else
            {
                Debug.Log("골드가 부족하셔셩");
            }    
        }        
    }

    public void ActiveSlot()
    {
        Unactive.gameObject.SetActive(false);        
    }

    public void UnActiveSlot()//대기상태
    {        
        Unactive.gameObject.SetActive(true);
    }

    public void UpBtnOn()
    {     
        Btn_Waiting.gameObject.SetActive(false);
    }

    public void UpBtnOff()
    { 
        Btn_Waiting.gameObject.SetActive(true);
    }

    public void PurchaseBtnOn()
    {  
        Btn_RecruitWaiting.gameObject.SetActive(false);
    }

    public void PurchaseBtnOff()
    {
        Btn_RecruitWaiting.gameObject.SetActive(true);
    }
}
