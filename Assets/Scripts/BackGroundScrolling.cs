using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BackGroundScrolling : MonoBehaviour
{
    //빌딩 sprite 교체를 위한 변수들
    Vector3[] buildingPos = new Vector3[3];
    public GameObject[] buildings = new GameObject[3];

    //움직임을 위한 변수들
    public GameObject[] moveAnkers = new GameObject[3];
    Vector3[] moveAnkerFirstPos = new Vector3[3];
    public float speed = 0;
    float[] movSpeed = new float[3];
    [SerializeField] int state = 0;
    WaitForSeconds wait= new WaitForSeconds(0.01f);

    void Start()
    {
        Initializing_BackGroundScrolling();
    }


    public void MoveBackGround()//이전 건물 클리어 후
    {
        StopAllCoroutines();
        StartCoroutine(MoveBackGroundCoroutine());
    }

    IEnumerator MoveBackGroundCoroutine()
    {
        Color color = buildings[state].transform.GetComponent<SpriteRenderer>().color;
        while (color.a > 0.02f)
        {
            color.a -= 0.01f;
            buildings[state].transform.GetComponent<SpriteRenderer>().color = color;
            yield return wait;
        }

        if (state == 2)
        {
            //페이드인&아웃
            
            state = 0;
            for (int i = 0; i < 3; i++)
            {
                moveAnkers[i].transform.position = moveAnkerFirstPos[i];
            }

            

            for (int i = 0; i < 3; i++)
            {
                Color color1 = buildings[i].transform.GetComponent<SpriteRenderer>().color;
                color1.a = 1;
                buildings[i].gameObject.GetComponent<SpriteRenderer>().color = color1;
                //buildings[i].gameObject.GetComponent<SpriteRenderer>().sprite = Resources.Load<Sprite>("currstage에 따른 성 이미지");
            }
        }

        // state가 2이면 0으로 돌리고 최초 위치로 이동. 그리고 0이 된 알파값들 원상복구 후, 스테이지 데이터에서 스테이지 번호 받아와서 건물 스프라이트 변경.
        else
        {
            state++;
            while (buildings[state].transform.position.x - buildingPos[0].x > 0.05f)
            {
                for (int i = 0; i < 3; i++)
                {
                    moveAnkers[i].transform.Translate(Vector3.left * movSpeed[i]);
                    yield return wait;
                }
            }
        }        
    }
    public void Initializing_BackGroundScrolling()
    {
        for (int i = 0; i < 3; i++)
        {
            buildingPos[i] = buildings[i].transform.position;
            moveAnkerFirstPos[i] = moveAnkers[i].transform.position;
        }
        movSpeed[0] = speed;
        movSpeed[1] = speed * 0.85f;
        movSpeed[2] = speed * 0.7f;

        
    }
}