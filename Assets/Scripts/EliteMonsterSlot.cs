using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class EliteMonsterSlot : MonoBehaviour
{

    DataBaseManager thedata;
    CurrencyManager thecurrency;
    MonsterProduct theProduct;
    UserDataController theUser;
    SlotController theSlot;
    SliderController theSlider;


    //������ ��ư
    public bool isBtnDowning = false;
    public float levelUPCoolTime = 0.5f;
    public float levelUpWaitSecond = 0;

    //UI��
    public GameObject Active, Unactive;
    public Slider slider;
    public TextMeshProUGUI tm_Name, tm_Title, tm_Lv, tm_Atk, tm_currPrice;    
    public TextMeshProUGUI Txt_level, Txt_duration, Txt_firstprice;
    public TextMeshProUGUI txt_UpgradePrice;

    public Button Btn_Upgrade, Btn_Waiting;
    public Button Btn_Recruit, Btn_RecruitWaiting;

    public int currLevel, grade = 0;
    public string st_name, st_title;
    public float[] initialAtk, currAtk, atkAddValue = new float[2];       
    public float[] initialPrice, currPrice, priceAddValue = new float[2];
    public float duration = 0;    
    public Image background, mdl;   
    public int slotIndex;

    public int currCardCount, needCardCount;


    void Start()
    {
        thedata = FindObjectOfType<DataBaseManager>();
        thecurrency = FindObjectOfType<CurrencyManager>();
        theProduct = FindObjectOfType<MonsterProduct>();
        theUser = FindObjectOfType<UserDataController>();
        theSlot = FindObjectOfType<SlotController>();
        theSlider = FindObjectOfType<SliderController>();

        SetInitialData();        
        FillText();
    }

    void Update()
    {
        if (isBtnDowning)
        {
            if (levelUpWaitSecond <= 0)
            {
                LevelUp();
                if (levelUPCoolTime <= 0.05f)
                {
                    levelUPCoolTime = 0.05f;
                }
                else
                {
                    levelUPCoolTime -= 0.05f;
                }
                levelUpWaitSecond = levelUPCoolTime;
            }
            else
            {
                levelUpWaitSecond -= Time.deltaTime;
            }
        }
    }

    public void PointerDown()
    {
        isBtnDowning = true;
    }

    public void PointerUp()
    {
        isBtnDowning = false;
    }

    public void SetInitialData()
    {
        background.sprite = Resources.Load<Sprite>("images/bg/" + grade.ToString());
        mdl.sprite = Resources.Load<Sprite>("images/elite/" + (100 + slotIndex).ToString());

        currLevel = theUser.a_eliteMonsterSlotLv[slotIndex];
        grade = (int)thedata.EliteMonsterData[slotIndex]["eliteMonsterGrade"];        
        st_title = thedata.EliteMonsterData[slotIndex]["eliteMonsterName"].ToString();
        st_name = thedata.EliteMonsterData[slotIndex]["eliteMonsterTitle"].ToString();

        object obj = thedata.EliteMonsterData[slotIndex]["eliteMonsterAtk"];
        if(obj.GetType().Name == "Int32")
        {
            initialAtk[0] = (int)thedata.EliteMonsterData[slotIndex]["eliteMonsterAtk"];
        }
        else
        {
            initialAtk[0] = (float)thedata.EliteMonsterData[slotIndex]["eliteMonsterAtk"];
        }

        obj = thedata.EliteMonsterData[slotIndex]["eliteMonsterAtkLog10"];
        if (obj.GetType().Name == "Int32")
        {
            initialAtk[1] = (int)thedata.EliteMonsterData[slotIndex]["eliteMonsterAtkLog10"];
        }
        else
        {
            initialAtk[1] = (float)thedata.EliteMonsterData[slotIndex]["eliteMonsterAtkLog10"];
        }        
        atkAddValue[0] = initialAtk[0];
        atkAddValue[1] = initialAtk[1] - 1;

        obj = thedata.EliteMonsterData[slotIndex]["eliteMonsterOrb"];
        if (obj.GetType().Name == "Int32")
        {
            initialPrice[0] = (int)thedata.EliteMonsterData[slotIndex]["eliteMonsterOrb"];
        }
        else
        {
            initialPrice[0] = (float)thedata.EliteMonsterData[slotIndex]["eliteMonsterOrb"];
        }

        obj = thedata.EliteMonsterData[slotIndex]["eliteMonsterOrbLog10"];
        if (obj.GetType().Name == "Int32")
        {
            initialPrice[1] = (int)thedata.EliteMonsterData[slotIndex]["eliteMonsterOrbLog10"];
        }
        else
        {
            initialPrice[1] = (float)thedata.EliteMonsterData[slotIndex]["eliteMonsterOrbLog10"];
        }

        priceAddValue[0] = initialPrice[0];
        priceAddValue[1] = initialPrice[1];

        obj = thedata.EliteMonsterData[slotIndex]["eliteMonsterCoolTime"];
        if (obj.GetType().Name == "Int32")
        {
            duration = (int)thedata.EliteMonsterData[slotIndex]["eliteMonsterCoolTime"];
        }
        else
        {
            duration = (float)thedata.EliteMonsterData[slotIndex]["eliteMonsterCoolTime"];
        }
        CalcurateCurrAtk();
        CalcurateCurrPrice();
    }

    void CalcurateCurrAtk()
    {
        currAtk = thecurrency.Multiple(atkAddValue, currLevel);
        currAtk = thecurrency.Plus(initialAtk, currAtk);
    }

    void CalcurateCurrPrice()
    {
        currPrice = thecurrency.Multiple(priceAddValue, currLevel);
        currPrice = thecurrency.Plus(initialPrice, currAtk);
    }    
    

    public void FillText()
    {
        tm_Name.text = st_name;
        tm_Title.text = st_title;
        tm_Lv.text = "Lv." + currLevel.ToString();
        tm_Atk.text = "Atk EPx" + thecurrency.NumberToString(currAtk);
        Txt_duration.text = "Duration" + duration.ToString() + "s";
        tm_currPrice.text = thecurrency.NumberToString(currPrice);
    }

    public void FillTextLevelUp()//�̸��� Ÿ��Ʋ ������ �ؽ�Ʈ ����
    {
        tm_Lv.text = "Lv." + currLevel.ToString();
        tm_Atk.text = "Atk EPx" + thecurrency.NumberToString(currAtk);
        Txt_duration.text = "Duration" + duration.ToString() + "s";
        tm_currPrice.text = thecurrency.NumberToString(currPrice);
    }

    public void LevelUp()
    {
        int a = thecurrency.CompareFloat(currPrice, theUser.currGold);

        if (a == 1 || a == 2)
        {
            theUser.SpendOrb(currPrice);
            currLevel++;
            theUser.a_eliteMonsterSlotLv[slotIndex] = currLevel;
            CalcurateCurrPrice();
            CalcurateCurrAtk();
            FillTextLevelUp();
        }

        else
        {
            Debug.Log("��尡 �����ϼż�");
        }
    }    

    public void BuySlot()
    {
        if (theUser.monsterSlotActiveNumber == 29)//�������� ������. ��ư�� ���� ������
        {
            Debug.Log("��� ���� �������Դϴ�");
        }
        else
        {
            int a = thecurrency.CompareFloat(currPrice, theUser.currOrb);
            if (a == 1 || a == 2)
            {
                theUser.SpendOrb(currPrice);
                theSlot.BuyEliteMonsterSlot(slotIndex);
                theSlider.MakeEliteMonsterChecker(slotIndex + 1);
            }
            else
            {
                Debug.Log("��尡 �����ϼż�");
            }
        }        
    }

    public void ActiveSlot()
    {       
        Unactive.gameObject.SetActive(false);
    }

    public void UnActiveSlot()
    {       
        Unactive.gameObject.SetActive(true);
    }

    public void UpBtnOn()
    {
        Btn_Waiting.gameObject.SetActive(false);
    }

    public void UpBtnOff()
    {
        Btn_Waiting.gameObject.SetActive(true);
    }

    public void PurchaseBtnOn()
    {
        Btn_RecruitWaiting.gameObject.SetActive(false);
    }

    public void PurchaseBtnOff()
    {
        Btn_RecruitWaiting.gameObject.SetActive(true);
    }
}
