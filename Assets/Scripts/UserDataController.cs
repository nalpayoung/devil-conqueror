using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UserDataController : MonoBehaviour
{
    public static UserDataController instance;
    DataBaseManager theData;
    CurrencyManager theCurrency;
    UpperUI theUpperUI;
    EquipmentSlotChecker theEquipSlots;

    //기본 정보
    float n_monsterMoveSpd = 6;
    float e_monsterMoveSpd = 6;

    //재화 정보
    public float[] currGold = new float[2];
    public float[] currStone = new float[2];//장비강화석
    public int currGem = 0;
    public int currBadge = 0;//엘리트강화석
    public float[] relicShards = new float[2];//유물강화재료
    public float[] currOrb = new float[2]; //마력강화재료
    public float maxMp = 0;

    //몬스터 슬롯 정보
    public int[] a_normalMonsterSlotLv = new int[30];
    public int[] a_eliteMonsterSlotLv = new int[30];
    public int monsterSlotActiveNumber;
    public int eliteSlotActiveNumber;
    public float[] a_normalMonsterAtkAddFromElite = new float[2];

    //장비 정보
    public int equipedArmorNo;
    public int equipedWeaponNo;
    public bool[] armorActivate = new bool[24];
    public bool[] weaponActivate = new bool[24];
    public int[] armorCount = new int[24];//개수
    public int[] weaponCount = new int[24];
    public int[] armorLv = new int[24];
    public int[] weaponLv = new int[24];    
    public float[] weaponAtkIncrease = new float[2];
    public float weaponCritDamageAdd = 0;
    public float weaponGoldEarnAdd = 0;
    public float[] armorHpIncrease = new float[2];
    public float armorManaIncrease = 0;
    public float armorExpAdd = 0;

    //유물 정보
    public int[] relicLv = new int[13];
    public bool[] relicActiveList = new bool[13];
    public int[] relicEarnedCount = new int[13];
    public float r_monsterAtk = 0;
    public float r_eliteAtk = 0, r_evilPower, r_meteoDmg, r_goldEarning, r_enemyHpminus, r_blizzardDmg, r_monsterHp = 0;
    public float r_monsterSpd, r_eliteCooltime, r_eliteSpd, r_eliteDuration, r_monsterCooltime = 0;
    public int relicSummonCount = 0;

    //스킬 정보
    public int[] skillLv = new int[5];
    public bool[] isSkillActive = new bool[5];
    public float s_stageGoldEarning = 0;
    public float s_monsterAtkSpd = 0;
    public float s_monsterCooltimeMinus = 0;
    public float[] s_blizzardDmg = new float[2];
    public float[] s_meteoDmg = new float[2];

    //마력 정보
    public int evilPowerLv = 0;
    public float[] evilPowerValue = new float[2];
    public float[] evilPowerBuffValue = new float[2];

    //스테이지 정보
    public int currStage = 0;

    //타이틀 정보
    public int currTitleNo = 0;
    public float[] t_AtkAdd = new float[2];
    public float[] t_HpAdd = new float[2];

    //최후정보
    public float[] normalMonsterAtkAdd = new float[2];//
    public float[] normalMnosterHpAdd = new float[2];//
    public float n_monsterMoveSpdAdd, n_monsterCoolTimeMinus = 0;
    public float atkSpdAdd = 0;
    public float[] eliteMonsterAtkAdd = new float[2];
    public float e_eliteMoveSpdAdd, e_eliteCoolTimeMinus, e_eliteDrationAdd = 0;
    public float goldEarningAdd_all = 0;
    public float[] goldEarningAdd_Stage = new float[2];
    public float meteoDmgAdd, blizzardDmbAdd, enemyCastleHpMinus;

    void Start()
    {
        instance = this;
        theData = FindObjectOfType<DataBaseManager>();
        theCurrency = FindObjectOfType < CurrencyManager>();
        theUpperUI = FindObjectOfType<UpperUI>();
        theEquipSlots = FindObjectOfType<EquipmentSlotChecker>();
    }

    public void LoadFromDatabase()//데이터베이스에서 데이터 받아오기
    {
        //재화 데이터
        currGold = theData.currGold;
        currGem = theData.currGem;
        currStone = theData.currStone;
        currBadge = theData.currBadge;
        relicShards = theData.relicShards;
        currOrb = theData.orb;
        maxMp = theData.maxMp;
        //몬스터정보
        a_normalMonsterSlotLv = theData.a_eliteMonsterSlotLv;
        a_eliteMonsterSlotLv = theData.a_eliteMonsterSlotLv;
        monsterSlotActiveNumber = theData.monsterSlotActiveNumber;
        eliteSlotActiveNumber = theData.eliteSlotActiveNumber;
        //장비정보
        armorActivate = theData.armorActivate;
        weaponActivate = theData.weaponActivate;
        equipedWeaponNo = theData.equipedWeaponNo;
        equipedWeaponNo = theData.equipedWeaponNo;
        armorCount = theData.armorCount;
        weaponCount = theData.weaponCount;
        armorLv = theData.armorLv;
        weaponLv = theData.weaponLv;        
        //유물정보
        relicLv = theData.relicLv;
        relicActiveList = theData.relicActiveList;
        relicEarnedCount = theData.relicEarnedCount;
        relicSummonCount = theData.relicSummonCount;
        //스킬정보
        skillLv = theData.skillLv;
        //마력정보
        evilPowerLv = theData.evilPowerLv;
        //스테이지정보
        currStage = theData.currStage;
        //타이틀정보
        currTitleNo = theData.currTitleNo;
    }

    public void SaveToDatabase()
    {

    }

    public void Initialize()
    {
        StartCoroutine(InitializeCoroutine());
    }

    IEnumerator InitializeCoroutine()
    {
        yield return StartCoroutine(CalcurateEquipmentAdd());
        yield return StartCoroutine(CalcurateRelicAdd());
        yield return StartCoroutine(CalcurateSkillValue());
        yield return StartCoroutine(CalcurateTitleValue());
        yield return StartCoroutine(CalcurateEvilPower());
        yield return StartCoroutine(CalcurateMonsterAtkAdd());
        yield return StartCoroutine(CalcurateMonsterHpAdd());
        yield return StartCoroutine(CalcurateEliteAtkAdd());
        yield return StartCoroutine(CalcurateAllTerminalData());        
    }


    IEnumerator CalcurateEquipmentAdd()//장비 버프 계산
    {
        //weaponAtkIncrease        
        float value = 0;
        float sum = 0;
        for (int i = 0; i < weaponLv.Length; i++)
        {
            if(weaponActivate[i]) 
            {
                value = (float)theData.WeaponUnequipedAtkAddData[weaponLv[i]][i.ToString()];
                sum += value;
            }
            else { continue; }            
        }
        sum += (float)theData.WeaponEquipedAtkAddData[weaponLv[equipedWeaponNo]][equipedWeaponNo.ToString()];
        weaponAtkIncrease = CurrencyManager.instance.Currencying(sum);

        //weaponCritDamageAdd
        if(equipedWeaponNo > 7)
        {
            weaponCritDamageAdd = (float)theData.WeaponData[equipedWeaponNo]["weaponCritDamage"]
            + (float)theData.WeaponData[equipedWeaponNo]["weaponCritDamage"] * 0.1f * weaponLv[equipedWeaponNo];
        }
        else
        {
            weaponCritDamageAdd = 0;
        }        

        //weaponGoldEarnAdd
        if(equipedWeaponNo > 19)
        {
            weaponGoldEarnAdd = (float)theData.WeaponData[equipedWeaponNo]["weaponGoldEarn"]
            + (float)theData.WeaponData[equipedWeaponNo]["weaponGoldEarn"] * 0.1f * weaponLv[equipedWeaponNo];
        }
        else
        {
            weaponGoldEarnAdd = 0;
        }

        //armorHpIncrease
        float value_a = 0;
        float sum_a = 0;
        for (int i = 0; i < armorLv.Length; i++)
        {
            if (armorActivate[i])
            {
                value_a = (float)theData.ArmorUnequipedHpAddData[armorLv[i]][i.ToString()];
                sum_a += value_a;
            }
            else continue;
        }
        sum_a += (float)theData.ArmorEquipedHpAddData[armorLv[equipedArmorNo]][equipedArmorNo.ToString()];
        armorHpIncrease = CurrencyManager.instance.Currencying(sum_a);

        //armorManaIncrease
        if (equipedArmorNo > 7)
        {
            armorManaIncrease = (float)theData.ArmorData[equipedArmorNo]["armorManaRecovery"]
            + (float)theData.ArmorData[equipedArmorNo]["armorManaRecovery"] * 0.1f * armorLv[equipedArmorNo];
        }
        else armorManaIncrease = 0;

        //armorExpAdd
        if (equipedArmorNo > 19)
        {
            armorExpAdd = (float)theData.ArmorData[equipedArmorNo]["armorExp"]
                + (float)theData.ArmorData[equipedArmorNo]["armorExp"] * 0.1f * armorLv[equipedArmorNo];
        }
        else armorExpAdd = 0;

        yield return null;
    }//장비 능력치 종합 계산
    IEnumerator CalcurateRelicAdd()//유물 버프 계산
    {
        r_monsterAtk = (float)theData.RelicData[0]["RelicValuePerLevel"] * theData.relicLv[0];
        r_monsterSpd = (float)theData.RelicData[1]["RelicValuePerLevel"] * theData.relicLv[1];
        r_eliteAtk = (float)theData.RelicData[2]["RelicValuePerLevel"] * theData.relicLv[2];
        r_eliteCooltime = (float)theData.RelicData[3]["RelicValuePerLevel"] * theData.relicLv[3];
        r_eliteSpd = (float)theData.RelicData[4]["RelicValuePerLevel"] * theData.relicLv[4];
        r_eliteDuration = (float)theData.RelicData[5]["RelicValuePerLevel"] * theData.relicLv[5];
        r_evilPower = (float)theData.RelicData[6]["RelicValuePerLevel"] * theData.relicLv[6];
        r_meteoDmg = (float)theData.RelicData[7]["RelicValuePerLevel"] * theData.relicLv[7];
        r_goldEarning = (float)theData.RelicData[8]["RelicValuePerLevel"] * theData.relicLv[8];
        r_enemyHpminus = (float)theData.RelicData[9]["RelicValuePerLevel"] * theData.relicLv[9];
        r_blizzardDmg = (float)theData.RelicData[10]["RelicValuePerLevel"] * theData.relicLv[10];
        r_monsterCooltime = (float)theData.RelicData[11]["RelicValuePerLevel"] * theData.relicLv[11];
        r_monsterHp = (float)theData.RelicData[12]["RelicValuePerLevel"] * theData.relicLv[12];
        yield return null;
    } //유물 능력치 수치 계산

    public void CalcurateRelic()
    {
        StopCoroutine(CalcurateRelicAdd());
        StartCoroutine(CalcurateRelicAdd());
    }
    IEnumerator CalcurateSkillValue()//스킬 능력치 수치 계산
    {
        s_stageGoldEarning = (float)theData.SkillData[0]["skillFirstEff"] + (float)theData.SkillData[0]["skillIncreaseEff"] * skillLv[0];
        s_monsterAtkSpd = (int)theData.SkillData[1]["skillFirstEff"] + (float)theData.SkillData[1]["skillIncreaseEff"] * skillLv[1];
        s_monsterCooltimeMinus = (int)theData.SkillData[2]["skillFirstEff"] + (float)theData.SkillData[2]["skillIncreaseEff"] * skillLv[2];
        float blizaardValue = (float)theData.SkillData[3]["skillFirstEff"] + (float)theData.SkillData[3]["skillIncreaseEff"] * skillLv[3];
        s_blizzardDmg = CurrencyManager.instance.Currencying(blizaardValue);
        float meteoValue = (float)theData.SkillData[4]["skillFirstEff"] + (float)theData.SkillData[3]["skillIncreaseEff"] * skillLv[4];
        s_meteoDmg = CurrencyManager.instance.Currencying(meteoValue);

        yield return null;
    }

    IEnumerator CalcurateTitleValue()//타이틀 수치 계산
    {
        float t_atkvalue = (float)theData.TitleData[currTitleNo]["titleAtkAdd"];
        float t_hpvalue = (float)theData.TitleData[currTitleNo]["titleHpAdd"];
        t_AtkAdd = CurrencyManager.instance.Currencying(t_atkvalue);
        t_HpAdd = CurrencyManager.instance.Currencying(t_hpvalue);

        yield return null;
    }

    IEnumerator CalcurateEvilPower()//EvilPower로부터 비롯되는 수치들 계산
    {
        float evilPowerValueNoCurrencying = 0;
        float evilPowerBuffValueNoCurrencying = 0;

        if(evilPowerLv >= 10)
        {
            if(evilPowerLv >= 100)
            {
                if(evilPowerLv >= 1000)
                {
                    if(evilPowerLv >= 10000)
                    {
                        evilPowerValueNoCurrencying = 5 * evilPowerLv;
                        evilPowerBuffValueNoCurrencying = 500 * evilPowerLv;
                    }
                    else
                    {
                        evilPowerValueNoCurrencying = 4 * evilPowerLv;
                        evilPowerBuffValueNoCurrencying = 40 * evilPowerLv;
                    }
                }
                else
                {
                    evilPowerValueNoCurrencying = 3 * evilPowerLv;
                    evilPowerBuffValueNoCurrencying = 3 * evilPowerLv;
                }
            }
            else
            {
                evilPowerValueNoCurrencying = 2 * evilPowerLv;
                evilPowerBuffValueNoCurrencying = 0.2f * evilPowerLv;
            }            
        }
        else
        {
            evilPowerValueNoCurrencying = evilPowerLv;
        }
        evilPowerValue = CurrencyManager.instance.Currencying(evilPowerValueNoCurrencying);
        evilPowerBuffValue = CurrencyManager.instance.Currencying(evilPowerBuffValueNoCurrencying);

        yield return null;
    }

    IEnumerator CalcurateMonsterAtkAdd() //최종 몬스터 공격력 증가치 계산
    {
        float[] one = new float[] { 1f, 0f };

        float[] n_weaponIncrease = CurrencyManager.instance.Plus(one, weaponAtkIncrease);
        float[] n_evilPowerIncrease = CurrencyManager.instance.Plus(one, evilPowerBuffValue);
        float[] n_eliteIncrease = CurrencyManager.instance.Plus(one, a_normalMonsterAtkAddFromElite);
        float n_relicIncrease = 1 + r_monsterAtk;
        float[] n_titleIcrease = CurrencyManager.instance.Plus(one, t_AtkAdd);

        float[] calcurateArray = CurrencyManager.instance.Multiple(n_weaponIncrease, n_eliteIncrease);
        calcurateArray = CurrencyManager.instance.Multiple(calcurateArray, n_evilPowerIncrease);
        calcurateArray = CurrencyManager.instance.Multiple(calcurateArray, n_relicIncrease);
        calcurateArray = CurrencyManager.instance.Multiple(calcurateArray, n_titleIcrease);

        normalMonsterAtkAdd = calcurateArray;

        yield return null;
    }

    IEnumerator CalcurateMonsterHpAdd() //최종 몬스터 체력 증가치 계산
    {
        float[] one = new float[] { 1f, 0f };

        float[] n_weaponIncrease = CurrencyManager.instance.Plus(one, armorHpIncrease);
        float[] n_evilPowerIncrease = CurrencyManager.instance.Plus(one, evilPowerBuffValue);
        float[] n_eliteIncrease = CurrencyManager.instance.Plus(one, a_normalMonsterAtkAddFromElite);
        float n_relicIncrease = 1 + r_monsterHp;
        float[] n_titleIcrease = CurrencyManager.instance.Plus(one, t_HpAdd);

        float[] calcurateArray = CurrencyManager.instance.Multiple(n_weaponIncrease, n_eliteIncrease);
        calcurateArray = CurrencyManager.instance.Multiple(calcurateArray, n_evilPowerIncrease);
        calcurateArray = CurrencyManager.instance.Multiple(calcurateArray, n_relicIncrease);
        calcurateArray = CurrencyManager.instance.Multiple(calcurateArray, n_titleIcrease);
        normalMnosterHpAdd = calcurateArray;

        yield return null;
    }

    IEnumerator CalcurateEliteAtkAdd()//제네럴 공격력 계산 CalcurateEvilPower 먼저 돌려야 함, 제네럴의 공격력을 곱해야함
    {
        float[] one = new float[] { 1f, 0f };
        CurrencyManager.instance.Multiple(evilPowerValue, 1+ r_eliteAtk);

        yield return null;
    }//마력*(1+유물_제네럴공격력증가)

    IEnumerator CalcurateAllTerminalData()
    {        
        n_monsterMoveSpdAdd = r_monsterSpd;
        n_monsterCoolTimeMinus = r_monsterCooltime;//스킬에서 처리
        atkSpdAdd = 0; //스킬에서 처리해야함. 왜냐하면 켜질때 체크를 위해서
        e_eliteMoveSpdAdd = r_eliteSpd;
        e_eliteCoolTimeMinus = r_eliteCooltime;
        e_eliteDrationAdd = r_eliteDuration;
        goldEarningAdd_all = r_goldEarning;
        goldEarningAdd_Stage[0] = 0; //스킬에서 처리해야함. 왜냐하면 켜질때 체크를 위해서
        goldEarningAdd_Stage[1] = 0; //스킬에서 처리해야함. 왜냐하면 켜질때 체크를 위해서
        meteoDmgAdd = r_meteoDmg;
        blizzardDmbAdd = r_blizzardDmg;
        enemyCastleHpMinus = r_enemyHpminus;

        yield return null;
    }

    public void SpendGold(float[] _gold)
    {
        currGold = theCurrency.Minus(currGold, _gold);
        theUpperUI.GoldApply();
    }

    public void SpendOrb(float[] _orb)
    {
        currOrb = theCurrency.Minus(currOrb, _orb);
    }

    public void SpendStone(float[] _stone)
    {
        currStone = theCurrency.Minus(currStone, _stone);
    }

    public void SpendGem(int _gem)
    {
        currGem = currGem - _gem;
    }

    public void SpendShard(float[] _shard)
    {        
        theCurrency.Minus(relicShards, _shard);        
    }

    public void SpendBadge(int _badge)
    {
        currBadge -= _badge;
    }

    public void EarnGold(float[] _gold)
    {
        currGold = theCurrency.Plus(currGold, _gold);    
    }

    public void EarnOrb(float[] _orb)
    {
        currOrb = theCurrency.Plus(currOrb, _orb);
    }

    public void EarnStone(float[] _stone)
    {
        currStone = theCurrency.Plus(currStone, _stone);
    }

    public void EarnGem(int _gem)
    {
        currGem = currGem + _gem;
    }

    public void EarnShard(float[] _shard)
    {
        theCurrency.Plus(relicShards, _shard);
    }

    public void EarnBadge(int _badge)
    {
        currBadge += _badge;
    }

    public void EarnEquipment(int _type, int _no, int _count = 1)
    {
        if(_type == 0)
        {
            if (!weaponActivate[_no])
            {
                ActiveEquipment(_type, _no);
            }
            else
            {
                weaponCount[_no] += _count;
            }
            theEquipSlots.WeaponCheck();
        }
        else
        {
            if (!armorActivate[_no])
            {
                ActiveEquipment(_type, _no);
            }
            else
            {
                armorCount[_no] += _count;
            }
            theEquipSlots.ArmorCheck();
        }
        StopCoroutine(CalcurateEquipmentAdd());
        StartCoroutine(CalcurateEquipmentAdd());
    }

    public void ActiveEquipment(int _type, int _no)
    {
        if (_type == 0)
        {
            weaponActivate[_no] = true;
        }
        else
        {
            armorActivate[_no] = true;
        }
        StopCoroutine(CalcurateEquipmentAdd());
        StartCoroutine(CalcurateEquipmentAdd());
    }

    public void LevelUpEquipment(int _type, int _no)
    {
        if(_type == 0)
        {
            weaponLv[_no]++;
        }
        else
        {
            armorLv[_no]++;
        }
        StopCoroutine(CalcurateEquipmentAdd());
        StartCoroutine(CalcurateEquipmentAdd());        
    }

    public void ChangeEquipment(int _type, int _no)
    {
        if (_type == 0)
        {
            equipedWeaponNo = _no;
            theEquipSlots.WeaponEquipCheck();
        }
        else
        {
            equipedArmorNo = _no;
            theEquipSlots.ArmorEquipCheck();
        }
        StopCoroutine(CalcurateEquipmentAdd());
        StartCoroutine(CalcurateEquipmentAdd());
    }

    public void CombieEquipment(int _type, int _no)
    {
        if (_type == 0)
        {
            int a = weaponCount[_no] / 5;
            int b = weaponCount[_no] % 5;

            weaponCount[_no + 1] = weaponCount[_no + 1] + a;
            if (!weaponActivate[_no + 1])
            {
                weaponActivate[_no + 1] = true;
            }
            weaponCount[_no] = b;
            theEquipSlots.WeaponCheck();
        }
        else
        {
            int a = armorCount[_no] / 5;
            int b = armorCount[_no] % 5;

            armorCount[_no + 1] = armorCount[_no + 1] + a;
            if (!armorActivate[_no + 1])
            {
                armorActivate[_no + 1] = true;
            }
            armorCount[_no] = b;
            theEquipSlots.ArmorCheck();
        }
        StopCoroutine(CalcurateEquipmentAdd());
        StartCoroutine(CalcurateEquipmentAdd());
        
    }    
}

