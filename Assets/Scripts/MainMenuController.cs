using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MainMenuController : MonoBehaviour
{
    public GameObject[] Tabs;
    public GameObject[] Buttons;
    public GameObject[] MenuBtns;
   
    public void TapClick(int n)    
    {    
        for (int i = 0; i < Tabs.Length; i++)
        {
            if(i == n)
            {
                Tabs[i].SetActive(true);
                MenuBtns[i].SetActive(true);                
            }
            else
            {                
                Tabs[i].SetActive(false);                
                MenuBtns[i].SetActive(false);
            }            
        }       
    }
}
 