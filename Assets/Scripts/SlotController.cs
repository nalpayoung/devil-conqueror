using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SlotController : MonoBehaviour
{    
   
    CurrencyManager theCurrency;
    UserDataController theUser;

    public GameObject normalContent;//콘텐츠탭
    public GameObject EliteContent;                                

    //몬스터 슬롯
    public GameObject prefab_normalSlot;
    public GameObject prefab_EliteSlot;
    public GameObject prefab_Saibar;
    private GameObject Saibar;
    private GameObject Saibar1;

    //몬스터 슬롯 리스트
    public GameObject[] NormalMonsterSlotArray = new GameObject[30];
    public GameObject[] EliteMonsterSlotArray = new GameObject[30];
    
    public int monsterSlotActiveNo, eliteSlotActiveNo = 0;
    public float[] gold, orb = new float[2];

    WaitForSeconds wait = new WaitForSeconds(0.5f);

    bool goldChecking = true;
    bool orbChecking = true;
    public bool isCreatComplete = false;
    //활성화 여부 번호

    private void Start()
    {
        theUser = FindObjectOfType<UserDataController>();
        theCurrency = FindObjectOfType<CurrencyManager>();
       
    }

    private void Update()
    {
        if (isCreatComplete)
        {            
            if (!goldChecking)//if지금 보고 있는 화면이 일반몬스터 창일 경우
            {                
                goldChecking = true;
                GoldCheck();
            }
            //골드 가격 체크

            if (!orbChecking)
            {
                orbChecking = true;
                OrbCheck();
            }
        }        
    }

    public void CheckingBoolSetting()
    {
        isCreatComplete = true;
        goldChecking = false;
        if(theUser.eliteSlotActiveNumber != 0)
        {
            orbChecking = false;
        }
    }

    public void CreatSlot()//일단 만들어서 리스트에 넣기 UnActive 상태로//게임 시작시 사용
    {
        for (int i = 0; i < 30; i++)
        {            
            GameObject normalslot = Instantiate(prefab_normalSlot, normalContent.transform);
            NormalMonsterSlotArray[i] = normalslot;
            normalslot.SetActive(false);
            normalslot.GetComponent<MonsterSlot>().slotIndex = i;            
        }
        Saibar = Instantiate(prefab_Saibar, normalContent.transform);
        Saibar.SetActive(false);

        for (int i = 0; i < 30; i++)
        {
            GameObject Eliteslot = Instantiate(prefab_EliteSlot, EliteContent.transform);
            EliteMonsterSlotArray[i] = Eliteslot;
            Eliteslot.SetActive(false);
            Eliteslot.GetComponent<EliteMonsterSlot>().slotIndex = i;
        }
        Saibar1 = Instantiate(prefab_Saibar, EliteContent.transform);
        Saibar1.SetActive(false);
    }

    public void ActiveNormalSlot()//유저 데이터에서 정보 받아와서 상태 입력// 최초에 사용
    {
        monsterSlotActiveNo = theUser.monsterSlotActiveNumber;
        if (monsterSlotActiveNo == 29)
        {
            for (int i = 0; i < monsterSlotActiveNo + 1; i++)
            {
                NormalMonsterSlotArray[i].gameObject.SetActive(true);
                NormalMonsterSlotArray[i].GetComponent<MonsterSlot>().ActiveSlot();
            }
        }

        else
        {
            for (int i = 0; i < monsterSlotActiveNo + 2; i++)//Activates + 1만큼 활성화시키고, 그 다음건 비활성화상태로 비저블.
            {
                NormalMonsterSlotArray[i].gameObject.SetActive(true);

                if (i < monsterSlotActiveNo + 1)
                {
                    NormalMonsterSlotArray[i].GetComponent<MonsterSlot>().ActiveSlot();
                }

                else if (i == monsterSlotActiveNo + 1)
                {
                    NormalMonsterSlotArray[i].GetComponent<MonsterSlot>().UnActiveSlot();
                }
            }
        }

        if(monsterSlotActiveNo >= 3)
        {
            Saibar.SetActive(true);
        }
    }

    public void ActiveEliteSlot()//게임 시작시 유저정보에서 정보를 긁어와서 엘리트 슬롯 액티베이트//최초에 사용
    {
        eliteSlotActiveNo = theUser.eliteSlotActiveNumber;
        if (eliteSlotActiveNo == 29)
        {
            for (int i = 0; i < eliteSlotActiveNo + 1; i++)
            {
                EliteMonsterSlotArray[i].gameObject.SetActive(true);
                EliteMonsterSlotArray[i].GetComponent<EliteMonsterSlot>().ActiveSlot();
            }
        }

        else
        {
            for (int i = 0; i < eliteSlotActiveNo + 2; i++)//Activates + 1만큼 활성화시키고, 그 다음건 비활성화상태로 비저블.
            {
                EliteMonsterSlotArray[i].gameObject.SetActive(true);

                if (i < eliteSlotActiveNo + 1)
                {
                    EliteMonsterSlotArray[i].GetComponent<EliteMonsterSlot>().ActiveSlot();
                }

                else if (i == eliteSlotActiveNo + 1)
                {
                    EliteMonsterSlotArray[i].GetComponent<EliteMonsterSlot>().UnActiveSlot();
                }
            }
        }

        if (eliteSlotActiveNo >= 3)
        {
            Saibar1.SetActive(true);
        }
        
    }
   

    public void BuyNormalMonsterSlot(int _slotIndex)
    {        
        monsterSlotActiveNo++;
        theUser.monsterSlotActiveNumber = monsterSlotActiveNo;

        NormalMonsterSlotArray[monsterSlotActiveNo].GetComponent<MonsterSlot>().ActiveSlot();//해당 슬롯 열기

        NormalMonsterSlotArray[monsterSlotActiveNo + 1].SetActive(true);
        NormalMonsterSlotArray[monsterSlotActiveNo + 1].GetComponent<MonsterSlot>().UnActiveSlot();//다음슬롯 연 후 대기상태

        if (monsterSlotActiveNo == 3 && !Saibar.activeSelf)
        {
            Saibar.SetActive(true);
        }
    }

    public void BuyEliteMonsterSlot(int _slotIndex)
    {        
        eliteSlotActiveNo++;
        theUser.eliteSlotActiveNumber = eliteSlotActiveNo;

        EliteMonsterSlotArray[eliteSlotActiveNo].GetComponent<EliteMonsterSlot>().ActiveSlot();

        EliteMonsterSlotArray[eliteSlotActiveNo + 1].SetActive(true);
        EliteMonsterSlotArray[eliteSlotActiveNo + 1].GetComponent<EliteMonsterSlot>().UnActiveSlot();

        if (eliteSlotActiveNo == 3 && !Saibar.activeSelf)
        {
            Saibar.SetActive(true);
        }
    }

    public void GoldCheck()
    {
        StopCoroutine(GoldCheckCoroutine());
        StartCoroutine(GoldCheckCoroutine());
    }

    IEnumerator GoldCheckCoroutine()
    {
        gold = theUser.currGold;

        for (int i = 0; i < monsterSlotActiveNo + 1; i++)
        {
            int a = theCurrency.CompareFloat(NormalMonsterSlotArray[i].GetComponent<MonsterSlot>().currPrice, gold);            
            if(a == 1 || a == 2)
            {                
                NormalMonsterSlotArray[i].GetComponent<MonsterSlot>().UpBtnOn();
            }
            else
            {                
                NormalMonsterSlotArray[i].GetComponent<MonsterSlot>().UpBtnOff();
            }          
        }

        if(monsterSlotActiveNo != 29)
        {
            int b = theCurrency.CompareFloat(NormalMonsterSlotArray[monsterSlotActiveNo + 1].GetComponent<MonsterSlot>().currPrice, gold);

            if (b == 1 || b == 2)
            {
                NormalMonsterSlotArray[monsterSlotActiveNo + 1].GetComponent<MonsterSlot>().PurchaseBtnOn();
            }
            else
            {
                NormalMonsterSlotArray[monsterSlotActiveNo + 1].GetComponent<MonsterSlot>().PurchaseBtnOff();
            }
        }
        yield return wait;
        goldChecking = false;
    }

    public void OrbCheck()
    {
        StopCoroutine(OrbCheckCoroutine());
        StartCoroutine(OrbCheckCoroutine());
    }

    IEnumerator OrbCheckCoroutine()
    {
        orb = theUser.currOrb;

        for (int i = 0; i < eliteSlotActiveNo + 1; i++)
        {
            int a = theCurrency.CompareFloat(EliteMonsterSlotArray[i].GetComponent<EliteMonsterSlot>().currPrice, orb);
            if (a == 1 || a == 2)
            {
                EliteMonsterSlotArray[i].GetComponent<EliteMonsterSlot>().UpBtnOn();
            }
            else
            {
                EliteMonsterSlotArray[i].GetComponent<EliteMonsterSlot>().UpBtnOff();
            }
        }

        if (eliteSlotActiveNo != 29)
        {
            int b = theCurrency.CompareFloat(EliteMonsterSlotArray[eliteSlotActiveNo + 1].GetComponent<EliteMonsterSlot>().currPrice, orb);

            if (b == 1 || b == 2)
            {
                EliteMonsterSlotArray[eliteSlotActiveNo + 1].GetComponent<EliteMonsterSlot>().PurchaseBtnOn();
            }
            else
            {
                EliteMonsterSlotArray[eliteSlotActiveNo + 1].GetComponent<EliteMonsterSlot>().PurchaseBtnOff();
            }
        }
        yield return wait;
        orbChecking = false;
    }
}
