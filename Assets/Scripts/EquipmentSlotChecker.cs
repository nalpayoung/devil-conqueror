using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class EquipmentSlotChecker : MonoBehaviour
{
    public GameObject[] WeaponSlot;
    public GameObject[] ArmorSlot;
    public GameObject EquipmentPopUp;
    public UserDataController theUser;
    Vector3 initialPopUpPos;
    public TextMeshProUGUI tm_Equiped;

    public void Start()
    {
        WeaponCheck();
        ArmorCheck();        
    }
    public void OnEnable()
    {
        WeaponCheck();
        ArmorCheck();
    }

    public void WeaponCheck()//장비를 새로 입수했을 때 처리는 입수했을 시점 기준으로 이 로직을 돌려야 함
    {
        for (int i = 0; i < theUser.weaponActivate.Length; i++)
        {
            if (theUser.weaponActivate[i])
            {
                WeaponSlot[i].transform.GetChild(5).gameObject.SetActive(false);
            }
            WeaponSlot[i].transform.GetChild(2).GetComponent<TextMeshProUGUI>().text = "LV." + theUser.weaponLv[i].ToString();           
            WeaponSlot[i].transform.GetChild(3).GetComponent<TextMeshProUGUI>().text = theUser.weaponCount[i].ToString() + " / 5";
        }
        WeaponEquipCheck();
    }
    
    public void ArmorCheck()
    {
        for (int i = 0; i < theUser.armorActivate.Length; i++)
        {
            if (theUser.armorActivate[i])
            {
                ArmorSlot[i].transform.GetChild(5).gameObject.SetActive(false);
            }
            ArmorSlot[i].transform.GetChild(2).GetComponent<TextMeshProUGUI>().text = "LV." + theUser.armorLv[i].ToString();
            ArmorSlot[i].transform.GetChild(3).GetComponent<TextMeshProUGUI>().text = theUser.armorCount[i].ToString() + " / 5";
        }
        ArmorEquipCheck();
    }

    public void OpenWeaponWindow(int _no)
    {
        EquipmentPopUp.gameObject.GetComponent<EquipMentManager>().currType = 0;
        EquipmentPopUp.gameObject.GetComponent<EquipMentManager>().currNo = _no;
        EquipmentPopUp.transform.localPosition = Vector3.zero;
        EquipmentPopUp.gameObject.GetComponent<EquipMentManager>().AcitvOn();        
    }
     
    public void OpenArmorWindow(int _no)
    {
        EquipmentPopUp.gameObject.GetComponent<EquipMentManager>().currType = 1;
        EquipmentPopUp.gameObject.GetComponent<EquipMentManager>().currNo = _no;
        EquipmentPopUp.transform.localPosition = Vector3.zero;
        EquipmentPopUp.gameObject.GetComponent<EquipMentManager>().AcitvOn();        
    }   

    public void WeaponEquipCheck()
    {
        tm_Equiped.transform.SetParent(WeaponSlot[theUser.equipedWeaponNo].transform);
        tm_Equiped.transform.localPosition = new Vector3(66, 33, 0);
    }

    public void ArmorEquipCheck()
    {
        tm_Equiped.transform.SetParent(ArmorSlot[theUser.equipedArmorNo].transform);
        tm_Equiped.transform.localPosition = new Vector3(66, 33, 0);
    }
}
