using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class ArtifactManager : MonoBehaviour
{
    public UserDataController theuser;
    public DataBaseManager theData;
    CurrencyManager thecurrency;
    public GameObject[] RelicSlots;    
    public GameObject SaiBar;

    int[] needCardsLv200 = { 25, 50, 75, 100, 125, 150, 175, 200 };
    int[] needCards200 = { 2, 5, 10, 15, 20, 25, 30, 35 };
    int[] needCardLv500 = { 50, 100, 150, 200, 250, 300, 350, 400, 450, 500 };
    int[] needCard500 = { 2, 5, 8, 10, 13, 15, 23, 28, 32, 35 };

    bool isChecking;
    int activeCount = 0;
    int artifactLv;
    float artifactValuePerLv;
    string r_nameEn, r_nameKo, r_description;

    float[] artifactPrices;
    List<float[]> artifactPricesArray = new List<float[]>();
    float[] artifactValues;

    bool[] waitEvolve = new bool[12];

    WaitForSeconds time = new WaitForSeconds(1f);

    void Start()
    {        
        thecurrency = FindObjectOfType<CurrencyManager>();        
    }

    public void OnEnable()
    {
        InitializeArtifacts();
        SetArtifactPrice();               
        PurchasableCheck();
    }    

    public void InitializeArtifacts()
    {
        activeCount = theuser.relicSummonCount;
        SetArtifactValues();
        for (int i = 0; i < theuser.relicActiveList.Length; i++)
        {
            if (theuser.relicActiveList[i])
            {
                RelicSlots[i].gameObject.SetActive(true);                
                activeCount++;
                if (activeCount == 4)
                    SaiBar.gameObject.SetActive(true);
                
                FillArtifactData(i);
                EvolveBtnControl(i);
            }
            else
            {
                if (RelicSlots[i].activeSelf)
                {
                    RelicSlots[i].gameObject.SetActive(false);
                }
            }
        }                
    }

    public void SetArtifactPrice()//아티팩트 가격 채우기
    {
        float[] zero = { 0, 0 };
        for (int i = 0; i < theuser.relicActiveList.Length; i++)
        {
            if (theuser.relicActiveList[i])
            {                
                artifactPrices[i] = (int)theData.RelicData[i]["RelicPrice"] * 
                    Mathf.Pow((float)theData.RelicData[i]["RelicPriceMultiple"], theuser.relicLv[i]);

                artifactPricesArray.Add(thecurrency.Currencying(artifactPrices[i]));              
            }
            else
            {
                artifactPricesArray.Add(zero);
            }
        }
    }

    public void SetArtifactValues()
    {
        for (int i = 0; i < theuser.relicActiveList.Length; i++)
        {
            Debug.Log((float)theData.RelicData[i]["RelicValuePerLevel"] * theuser.relicLv[i]);
            artifactValues[i] = (float)theData.RelicData[i]["RelicValuePerLevel"] * theuser.relicLv[i];
        }
    }

    public void FillArtifactData(int _no)//처음
    {
        r_nameEn = theData.RelicData[_no]["RelicName"].ToString();
        r_nameKo = theData.RelicData[_no]["RelicNameKo"].ToString();
        r_description = theData.RelicData[_no]["RelicExplanation"].ToString();
        artifactLv = theuser.relicLv[_no];
        artifactValuePerLv = (float)theData.RelicData[_no]["RelicValuePerLevel"];

        string st_currValue = artifactValues[_no].ToString();
        string st_currPrice = thecurrency.NumberToString(artifactPricesArray[_no]);
        string st_earnedCount = theuser.relicEarnedCount[_no].ToString();

        RelicSlots[_no].transform.GetComponent<RelicSlot>().
            FillTexts(r_nameEn, artifactLv.ToString(), r_description, st_currValue, st_currPrice, st_earnedCount);
    }

    public void FillArtifactChangeData(int _no)//레벨업 등으로 데이터가 달라졌을 경우 적용
    {
        artifactLv = theuser.relicLv[_no];
        artifactValuePerLv = (float)theData.RelicData[_no]["RelicValuePerLevel"];

        string st_currValue = artifactValues[_no].ToString();        
        string st_currPrice = thecurrency.NumberToString(artifactPricesArray[_no]);
        string st_earnedCount = theuser.relicEarnedCount[_no].ToString();

        RelicSlots[_no].transform.GetComponent<RelicSlot>().
            FillTexts(r_nameEn, artifactLv.ToString(), r_description, st_currValue, st_currPrice, st_earnedCount);
    }

    public void EarnArtiFactCount(int _no)
    {
        if (theuser.relicActiveList[_no])
        {
            theuser.relicEarnedCount[_no]++;
            RelicSlots[_no].GetComponent<RelicSlot>().CountUp(theuser.relicEarnedCount[_no].ToString());
        }
        else
        {
            ActiveArtifact(_no);
        }
    }
    

    public void LevelUp(int _no)
    {
        //레벨업 처리        
        if (thecurrency.CompareFloat(theuser.relicShards, artifactPricesArray[_no]) == 0
            || thecurrency.CompareFloat(theuser.relicShards, artifactPricesArray[_no]) == 2)
        {
            theuser.SpendShard(artifactPricesArray[_no]);//shard 사용
            theuser.relicLv[_no]++;//유물레벨업
            artifactPrices[_no] = artifactPrices[_no] * (float)theData.RelicData[_no]["RelicPriceMultiple"];
            artifactPricesArray[_no] = thecurrency.Multiple(artifactPricesArray[_no],
                (float)theData.RelicData[_no]["RelicPriceMultiple"]);//유물가격 계산
            theuser.CalcurateRelic();//유물 수치 반영
            artifactValues[_no] = (float)theData.RelicData[_no]["RelicValuePerLevel"] * theuser.relicLv[_no];//유물다음레벨업계산
            FillArtifactChangeData(_no);//테스트채워넣기
            EvolveBtnControl(_no);
            PurchasableCheck();
        }

        else
        {
            Debug.Log("돈이 부족합니다");
        }
    }   

    

    public void EvolveBtnControl(int _no)//유물 획득할 때마다 체크하기
    {
        if (_no == 8 || _no == 11)
        {
            for (int i = 0; i < needCardLv500.Length; i++)
            {
                if (theuser.relicLv[_no] == needCardLv500[i])
                {
                    //Evolve 버튼 켜기
                    RelicSlots[_no].GetComponent<RelicSlot>().go_Evolve.gameObject.SetActive(true);
                    RelicSlots[_no].GetComponent<RelicSlot>().Tm_EarnedCount.text =
                        "(" + theuser.relicEarnedCount[_no].ToString() + "/" + needCard500[i].ToString() + ")";
                    if (theuser.relicEarnedCount[_no] > needCard500[i])
                    {
                        RelicSlots[_no].GetComponent<RelicSlot>().UnEvolve.gameObject.SetActive(false);
                    }
                    else
                    {
                        RelicSlots[_no].GetComponent<RelicSlot>().UnEvolve.gameObject.SetActive(true);
                    }
                    break;
                }
            }
        }
        else
        {
            for (int i = 0; i < needCardsLv200.Length; i++)
            {
                if (theuser.relicLv[_no] == needCardsLv200[i])
                {
                    RelicSlots[_no].GetComponent<RelicSlot>().go_Evolve.gameObject.SetActive(true);
                    RelicSlots[_no].GetComponent<RelicSlot>().Tm_EarnedCount.text =
                        "(" + theuser.relicEarnedCount[_no].ToString() + "/" + needCards200[i].ToString() + ")";
                    if (theuser.relicEarnedCount[_no] > needCards200[i])
                    {
                        RelicSlots[_no].GetComponent<RelicSlot>().UnEvolve.gameObject.SetActive(false);
                    }
                    else
                    {
                        RelicSlots[_no].GetComponent<RelicSlot>().UnEvolve.gameObject.SetActive(true);
                    }
                    break;
                }
            }
        }
        
    }

    public void EvolveArtifact(int _no)
    {
        
        if (_no == 8 || _no == 11)
        {
            for (int i = 0; i < needCardLv500.Length; i++)
            {
                if (theuser.relicLv[_no] == needCardLv500[i])
                {
                    theuser.relicEarnedCount[_no] -= needCard500[i];
                    RelicSlots[_no].GetComponent<RelicSlot>().Tm_EarnedCount.text =
                        "(" + theuser.relicEarnedCount[_no].ToString() + "/" + needCard500[i].ToString() + ")";
                    break;
                }                
            }
        }
        else
        {
            for (int i = 0; i < needCardsLv200.Length; i++)
            {
                if (theuser.relicLv[_no] == needCardsLv200[i])
                {
                    theuser.relicEarnedCount[_no] -= needCards200[i];
                    RelicSlots[_no].GetComponent<RelicSlot>().Tm_EarnedCount.text =
                        "(" + theuser.relicEarnedCount[_no].ToString() + "/" + needCards200[i].ToString() + ")";
                    break;
                }
            }
        }
        RelicSlots[_no].GetComponent<RelicSlot>().UnEvolve.gameObject.SetActive(true);
        RelicSlots[_no].GetComponent<RelicSlot>().go_Evolve.gameObject.SetActive(false);
    }

    public void ActiveArtifact(int _no)
    {
        RelicSlots[_no].gameObject.SetActive(true);

        theuser.relicActiveList[_no] = true;
        FillArtifactData(_no);
    }

    public void PurchasableCheck()//shard를 얻을때마다, 혹은 Enable로
    {
        float[] currShard = theuser.relicShards;
        for (int i = 0; i < artifactPricesArray.Count; i++)
        {
            if (theuser.relicActiveList[i])
            {
                if (thecurrency.CompareFloat(currShard, artifactPricesArray[i]) == 0 || thecurrency.CompareFloat(currShard, artifactPricesArray[i]) == 2)
                {
                    RelicSlots[i].GetComponent<RelicSlot>().UnActive.gameObject.SetActive(false);
                }
                else
                {
                    RelicSlots[i].GetComponent<RelicSlot>().UnActive.gameObject.SetActive(true);
                }

            }
        }
    }    

}
