using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class BtnController : MonoBehaviour, IPointerDownHandler, IPointerUpHandler
{
    MonsterSlot theSlot;
    bool isBtnDown = false;
    float seconds = 0.5f;
    float timeReduce = 0.9f;
    float timeMin = 0.05f;
    WaitForSeconds time = new WaitForSeconds(0.5f);
    SlotController theController;
    public Transform Parent;

    bool isCoroutine = false;

    void Start()
    {
        theSlot = Parent.GetComponent<MonsterSlot>();
        theController = FindObjectOfType<SlotController>();
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        isBtnDown = true;
        Debug.Log("True");
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        isBtnDown = false;
        Debug.Log("False");
    }

    IEnumerator LevelUpCoroutine()
    {
        theController.GoldCheck();
        theSlot.LevelUP();
        time = new WaitForSeconds(seconds);
        
        if(seconds > timeMin)
        {
            seconds = seconds * timeReduce;
        }

        yield return time;
        isCoroutine = false;        
    }
    

  
    void Update()
    {
        if (isBtnDown)
        {
            if (!isCoroutine)
            {
                isCoroutine = true;
                StartCoroutine(LevelUpCoroutine());
            }
        }        
    }
}
