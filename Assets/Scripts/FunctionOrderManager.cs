using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FunctionOrderManager : MonoBehaviour
{
    public SliderController theSliderController;
    public UserDataController theUser;
    public DataBaseManager theDatabase;
    public CurrencyManager theCurrency;
    public SlotController theSlot;
    public MonsterProduct theProduct;
    public UpperUI theUI;
    public MainMenuController theMain;

    void Start()
    {
        StartCoroutine(Initialize());        
    }

    IEnumerator Initialize()
    {
        yield return StartCoroutine(FillDatabase());        
        yield return StartCoroutine(FillUserData());
        yield return StartCoroutine(CreatMonsters());        
        yield return StartCoroutine(CreatSlot());
        yield return StartCoroutine(ActivateSliderController());
        yield return StartCoroutine(TapClickEverything());
    }

    IEnumerator FillDatabase()
    {
        theDatabase.LoadCsv();
        Debug.Log("CSV 로드 완료");
        theDatabase.SetTestData();//저장한 게 있다면 저장파일 로드//현재는 테스트
        Debug.Log("DB 초기화 완료");        
        yield return null;
    }    

    IEnumerator FillUserData()
    {        
        theUser.LoadFromDatabase();
        Debug.Log("유저 데이터 로드 완료");
        theUser.Initialize();
        Debug.Log("유저 데이터 초기화 완료");
        yield return new WaitForSeconds(1f);
        theUI.Initialize();
        Debug.Log("UI 초기화 완료");
    }   
  
    IEnumerator CreatMonsters()
    {        
        theProduct.CreatQueue();
        Debug.Log("몬스터 생성 완료");
        theProduct.CreatMonsters();
        Debug.Log("몬스터 생산 시작");
        yield return new WaitForSeconds(0.5f);
    }

    IEnumerator CreatSlot()
    {        
        theSlot.CreatSlot();
        Debug.Log("슬롯 생성 완료");

        theSlot.ActiveNormalSlot();
        Debug.Log("NormalSlot Activate Complete");

        theSlot.ActiveEliteSlot();
        Debug.Log("EliteSlot Activate Complete");

        theSlot.CheckingBoolSetting();
        Debug.Log("슬롯 업그레이드 Checker 작동 시작");
        yield return null;
    }

    IEnumerator ActivateSliderController()
    {
        theSliderController.Initialize();
        Debug.Log("슬라이더 초기화 완료");
        yield return null;
    }

    IEnumerator TapClickEverything()
    {
        yield return null;
        theMain.TapClick(1);
        Debug.Log("몬스터 화면으로 이동 완료");
    }

    void LoadData()//세이브 데이터를 데이터베이스로
    {
       
    }


    void DatabaseInitializing()//데이터베이스의 데이터를 각 오브젝트로
    {
       
    }

    void FillSlotData()//슬롯을 만들고 슬롯에 데이터베이스의 데이터를 입력
    {
        
    }

    void FillMonsterObjects()
    {
        
    }
   
}
