using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoomEffController : MonoBehaviour
{
    public GameObject[] boomObjects;

    IEnumerator StartBoomEffCoroutine()
    {
        for (int i = 0; i < boomObjects.Length - 1; i++)
        {
            boomObjects[i].gameObject.SetActive(true);
            yield return new WaitForSeconds(0.5f);
        }        
    }

    public void StartBoomEff()
    {
        StopAllCoroutines();
        StartCoroutine(StartBoomEffCoroutine());
    }

    IEnumerator EndBoomEffCoroutine()
    {
        for (int i = 0; i < boomObjects.Length - 1; i++)
        {
            boomObjects[i].gameObject.SetActive(false);
            yield return new WaitForSeconds(0.5f);
        }
    }

    public void EndBoomEff()
    {
        StopAllCoroutines();
        StartCoroutine(EndBoomEffCoroutine());
    }
}
