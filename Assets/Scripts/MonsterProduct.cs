using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MonsterProduct : MonoBehaviour
{   
    List<Queue<GameObject>> MonsterList = new List<Queue<GameObject>>();
    List<Queue<GameObject>> EliteMonsterList = new List<Queue<GameObject>>();
    public Transform[] start;    

    public void CreatQueue()
    {
        for (int i = 0; i < 30; i++)
        {
            Queue<GameObject> Monsters = new Queue<GameObject>();
            MonsterList.Add(Monsters);
        }

        for (int i = 0; i < 30; i++)
        {
            Queue<GameObject> Elite = new Queue<GameObject>();
            EliteMonsterList.Add(Elite);
        }
    }


    public void CreatMonsters()//몬스터, 엘리트 10개씩 만들기
    {
        for (int i = 0; i < 30; i++)
        {
            for (int j = 0; j < 10; j++)
            {
                GameObject normalcore = Instantiate(Resources.Load<GameObject>("prefab/normal/normalCore"));
                normalcore.transform.SetParent(transform);
                normalcore.GetComponent<MonsterMove>().slotIndex = i;

                GameObject monster = Instantiate(Resources.Load<GameObject>("prefab/normal/" + (200 + i).ToString()));
                monster.transform.SetParent(normalcore.transform);
                monster.transform.localPosition = Vector3.zero;

                normalcore.GetComponent<MonsterMove>().monsterType = 2;

                normalcore.SetActive(false);

                MonsterList[i].Enqueue(normalcore);
            }            
        }
        for (int i = 0; i < 30; i++)
        {
            for (int j = 0; j < 2; j++)
            {
                GameObject elitecore = Instantiate(Resources.Load<GameObject>("prefab/normal/normalCore"));
                elitecore.transform.SetParent(transform);
                elitecore.GetComponent<MonsterMove>().slotIndex = i;

                GameObject monster = Instantiate(Resources.Load<GameObject>("prefab/elite/" + (100 + i).ToString()));
                monster.transform.SetParent(elitecore.transform);
                monster.transform.localPosition = Vector3.zero;

                elitecore.GetComponent<MonsterMove>().monsterType = 1;

                elitecore.SetActive(false);

                EliteMonsterList[i].Enqueue(elitecore);
            }
        }
    }
    
    
    public void DeQueueMonster(int slotindex)
    {
        int a = Random.Range(0, 4);

        GameObject monster = MonsterList[slotindex].Dequeue();
        monster.transform.position = start[a].transform.position + new Vector3(0, 0, 0.2f * a);        
        monster.SetActive(true);       
    }

    public void DeQueueElite(int slotindex)
    {
        int a = Random.Range(0, 4);

        GameObject monster = EliteMonsterList[slotindex].Dequeue();
        monster.transform.position = start[a].transform.position + new Vector3(0, 0, 0.2f * a);
        monster.SetActive(true);
    }

    public void EnQueueMonster(GameObject monster)
    {
        int a = monster.GetComponent<MonsterMove>().slotIndex;
        MonsterList[a].Enqueue(monster);
        monster.SetActive(false);        
    } 

    public void EnqueueElite(GameObject monster)
    {
        int a = monster.GetComponent<MonsterMove>().slotIndex;
        EliteMonsterList[a].Enqueue(monster);
        monster.SetActive(false);
    }
}
