using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MonsterMove : MonoBehaviour
{
    public float moveSpeed;
    public int slotIndex;
    public int monsterType;
    bool EnqueueCheckCoroutine;
    MonsterProduct theProduct;

    private void Start()
    {
        theProduct = FindObjectOfType<MonsterProduct>();
    }

    void FixedUpdate()
    {
        transform.Translate(Vector3.right * moveSpeed * Time.deltaTime);
        if (!EnqueueCheckCoroutine)
        {
            EnqueueCheckCoroutine = true;
            EnqueueCheck();
        }        
    }

    void EnqueueCheck()
    {
        StartCoroutine(EnQueueCheckeCoroutine());
    }

    IEnumerator EnQueueCheckeCoroutine()
    {
        yield return new WaitForSeconds(1f);

        if (transform.position.x >= 5.5f)
        {
            if(monsterType == 2)
            {
                theProduct.EnQueueMonster(gameObject);
            }
            else
            {
                theProduct.EnqueueElite(gameObject);
            }
        }
        EnqueueCheckCoroutine = false;
    }
}
