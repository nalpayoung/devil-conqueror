using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CurrencyManager : MonoBehaviour
{
    public static CurrencyManager instance;
    
    Dictionary<int, string> currencyList = new Dictionary<int, string>();
    Dictionary<string, int> reversCurrencyList = new Dictionary<string, int>();

    void Awake()
    {
        if (null == instance)
        {            
            instance = this;
            DontDestroyOnLoad(this.gameObject);
        }
        else
        {            
            Destroy(this.gameObject);
        }
    }//싱글톤


    void Start()
    {
        FillCurrencyList();
    }

    void FillCurrencyList()
    {
        reversCurrencyList.Add("", 0);
        reversCurrencyList.Add("k", 1);
        reversCurrencyList.Add("m", 2);
        reversCurrencyList.Add("b", 3);
        reversCurrencyList.Add("t", 4);
        reversCurrencyList.Add("aa", 5);
        reversCurrencyList.Add("ab", 6);
        reversCurrencyList.Add("ac", 7);
        reversCurrencyList.Add("ad", 8);
        reversCurrencyList.Add("ae", 9);
        reversCurrencyList.Add("af", 10);
        reversCurrencyList.Add("ag", 11);
        reversCurrencyList.Add("ah", 12);
        reversCurrencyList.Add("ai", 13);
        reversCurrencyList.Add("aj", 14);
        reversCurrencyList.Add("ak", 15);
        reversCurrencyList.Add("al", 16);
        reversCurrencyList.Add("am", 17);
        reversCurrencyList.Add("an", 18);
        reversCurrencyList.Add("ao", 19);
        reversCurrencyList.Add("ap", 20);
        reversCurrencyList.Add("aq", 21);
        reversCurrencyList.Add("ar", 22);
        reversCurrencyList.Add("as", 23);
        reversCurrencyList.Add("at", 24);
        reversCurrencyList.Add("au", 25);
        reversCurrencyList.Add("av", 26);
        reversCurrencyList.Add("aw", 27);
        reversCurrencyList.Add("ax", 28);
        reversCurrencyList.Add("ay", 29);
        reversCurrencyList.Add("az", 30);
        reversCurrencyList.Add("ba", 31);
        reversCurrencyList.Add("bb", 32);
        reversCurrencyList.Add("bc", 33);
        reversCurrencyList.Add("bd", 34);
        reversCurrencyList.Add("be", 35);
        reversCurrencyList.Add("bf", 36);
        reversCurrencyList.Add("bg", 37);
        reversCurrencyList.Add("bh", 38);
        reversCurrencyList.Add("bi", 39);
        reversCurrencyList.Add("bj", 40);
        reversCurrencyList.Add("bk", 41);
        reversCurrencyList.Add("bl", 42);
        reversCurrencyList.Add("bm", 43);
        reversCurrencyList.Add("bn", 44);
        reversCurrencyList.Add("bo", 45);
        reversCurrencyList.Add("bp", 46);
        reversCurrencyList.Add("bq", 47);
        reversCurrencyList.Add("br", 48);
        reversCurrencyList.Add("bs", 49);
        reversCurrencyList.Add("bt", 50);
        reversCurrencyList.Add("bu", 51);
        reversCurrencyList.Add("bv", 52);
        reversCurrencyList.Add("bw", 53);
        reversCurrencyList.Add("bx", 54);
        reversCurrencyList.Add("by", 55);
        reversCurrencyList.Add("bz", 56);
        reversCurrencyList.Add("ca", 57);
        reversCurrencyList.Add("cb", 58);
        reversCurrencyList.Add("cc", 59);
        reversCurrencyList.Add("cd", 60);
        reversCurrencyList.Add("ce", 61);
        reversCurrencyList.Add("cf", 62);
        reversCurrencyList.Add("cg", 63);
        reversCurrencyList.Add("ch", 64);
        reversCurrencyList.Add("ci", 65);
        reversCurrencyList.Add("cj", 66);
        reversCurrencyList.Add("ck", 67);
        reversCurrencyList.Add("cl", 68);
        reversCurrencyList.Add("cm", 69);
        reversCurrencyList.Add("cn", 70);
        reversCurrencyList.Add("co", 71);
        reversCurrencyList.Add("cp", 72);
        reversCurrencyList.Add("cq", 73);
        reversCurrencyList.Add("cr", 74);
        reversCurrencyList.Add("cs", 75);
        reversCurrencyList.Add("ct", 76);
        reversCurrencyList.Add("cu", 77);
        reversCurrencyList.Add("cv", 78);
        reversCurrencyList.Add("cw", 79);
        reversCurrencyList.Add("cx", 80);
        reversCurrencyList.Add("cy", 81);
        reversCurrencyList.Add("cz", 82);
        reversCurrencyList.Add("da", 83);
        reversCurrencyList.Add("db", 84);
        reversCurrencyList.Add("dc", 85);
        reversCurrencyList.Add("dd", 86);
        reversCurrencyList.Add("de", 87);
        reversCurrencyList.Add("df", 88);
        reversCurrencyList.Add("dg", 89);
        reversCurrencyList.Add("dh", 90);
        reversCurrencyList.Add("di", 91);
        reversCurrencyList.Add("dj", 92);
        reversCurrencyList.Add("dk", 93);
        reversCurrencyList.Add("dl", 94);
        reversCurrencyList.Add("dm", 95);
        reversCurrencyList.Add("dn", 96);
        reversCurrencyList.Add("do", 97);
        reversCurrencyList.Add("dp", 98);
        reversCurrencyList.Add("dq", 99);
        reversCurrencyList.Add("dr", 100);
        reversCurrencyList.Add("ds", 101);
        reversCurrencyList.Add("dt", 102);


        currencyList.Add(0, "");
        currencyList.Add(1, "k");
        currencyList.Add(2, "m");
        currencyList.Add(3, "b");
        currencyList.Add(4, "t");
        currencyList.Add(5, "aa");
        currencyList.Add(6, "ab");
        currencyList.Add(7, "ac");
        currencyList.Add(8, "ad");
        currencyList.Add(9, "ae");
        currencyList.Add(10, "af");
        currencyList.Add(11, "ag");
        currencyList.Add(12, "ah");
        currencyList.Add(13, "ai");
        currencyList.Add(14, "aj");
        currencyList.Add(15, "ak");
        currencyList.Add(16, "al");
        currencyList.Add(17, "am");
        currencyList.Add(18, "an");
        currencyList.Add(19, "ao");
        currencyList.Add(20, "ap");
        currencyList.Add(21, "aq");
        currencyList.Add(22, "ar");
        currencyList.Add(23, "as");
        currencyList.Add(24, "at");
        currencyList.Add(25, "au");
        currencyList.Add(26, "av");
        currencyList.Add(27, "aw");
        currencyList.Add(28, "ax");
        currencyList.Add(29, "ay");
        currencyList.Add(30, "az");
        currencyList.Add(31, "ba");
        currencyList.Add(32, "bb");
        currencyList.Add(33, "bc");
        currencyList.Add(34, "bd");
        currencyList.Add(35, "be");
        currencyList.Add(36, "bf");
        currencyList.Add(37, "bg");
        currencyList.Add(38, "bh");
        currencyList.Add(39, "bi");
        currencyList.Add(40, "bj");
        currencyList.Add(41, "bk");
        currencyList.Add(42, "bl");
        currencyList.Add(43, "bm");
        currencyList.Add(44, "bn");
        currencyList.Add(45, "bo");
        currencyList.Add(46, "bp");
        currencyList.Add(47, "bq");
        currencyList.Add(48, "br");
        currencyList.Add(49, "bs");
        currencyList.Add(50, "bt");
        currencyList.Add(51, "bu");
        currencyList.Add(52, "bv");
        currencyList.Add(53, "bw");
        currencyList.Add(54, "bx");
        currencyList.Add(55, "by");
        currencyList.Add(56, "bz");
        currencyList.Add(57, "ca");
        currencyList.Add(58, "cb");
        currencyList.Add(59, "cc");
        currencyList.Add(60, "cd");
        currencyList.Add(61, "ce");
        currencyList.Add(62, "cf");
        currencyList.Add(63, "cg");
        currencyList.Add(64, "ch");
        currencyList.Add(65, "ci");
        currencyList.Add(66, "cj");
        currencyList.Add(67, "ck");
        currencyList.Add(68, "cl");
        currencyList.Add(69, "cm");
        currencyList.Add(70, "cn");
        currencyList.Add(71, "co");
        currencyList.Add(72, "cp");
        currencyList.Add(73, "cq");
        currencyList.Add(74, "cr");
        currencyList.Add(75, "cs");
        currencyList.Add(76, "ct");
        currencyList.Add(77, "cu");
        currencyList.Add(78, "cv");
        currencyList.Add(79, "cw");
        currencyList.Add(80, "cx");
        currencyList.Add(81, "cy");
        currencyList.Add(82, "cz");
        currencyList.Add(83, "da");
        currencyList.Add(84, "db");
        currencyList.Add(85, "dc");
        currencyList.Add(86, "dd");
        currencyList.Add(87, "de");
        currencyList.Add(88, "df");
        currencyList.Add(89, "dg");
        currencyList.Add(90, "dh");
        currencyList.Add(91, "di");
        currencyList.Add(92, "dj");
        currencyList.Add(93, "dk");
        currencyList.Add(94, "dl");
        currencyList.Add(95, "dm");
        currencyList.Add(96, "dn");
        currencyList.Add(97, "do");
        currencyList.Add(98, "dp");
        currencyList.Add(99, "dq");
        currencyList.Add(100, "dr");
        currencyList.Add(101, "ds");
        currencyList.Add(102, "dt");

    }


    public float[] Currencying(float _value) // 숫자를 [앞 세자리, 10의 배수]의 형태로 저장함
    {
        float[] value = new float[2];


        if(_value == 0)
        {
            value[0] = 0;
            value[1] = 0;
        }
        else
        {
            int digit = (int)System.Math.Truncate(Mathf.Log10(_value));
            if (digit < 0)
            {
                _value = _value * Mathf.Pow(10, Mathf.Abs(digit));
                digit = 0;
            }
            value[1] = digit;

            float number = _value / Mathf.Pow(10f, digit);
            string stNumber = number.ToString();
            int pointlocation = stNumber.IndexOf(".");

            switch (pointlocation)
            {
                case -1:
                    value[0] = number;
                    break;
                case 1:
                    stNumber = stNumber.Substring(0, 3);
                    value[0] = float.Parse(stNumber);
                    break;
            }
        }

       
        return value;
    }

    public string NumberToString(float[] _value) // [앞 세자리, 10의 배수] 형태로 저장된 데이터를 단위를 붙여 TXT로 바꿈
    {
        //9.6f, 2
        int quo = (int)_value[1] / 3;        
        int remain = (int)_value[1] % 3;        
        string digit = currencyList[quo];
        float number = _value[0] * Mathf.Pow(10, remain);
        number = (float)System.Math.Round(number, 2);

        return number.ToString() + digit;
    }

    public float[] Plus(float[] _value1, float[] _value2)//floating 된 두 수 더하기
    {
        float[] reValue;
        
        int digitdis = (int)Mathf.Abs(_value1[1] - _value2[1]);

        if(digitdis > 5)
        {
            if(_value1[1] - _value2[1] < 0)
            {
                return _value2;
            }
            else
            {
                return _value1;
            }
        }
        else
        {
            float number;
            if(_value1[1] - _value2[1] < 0)
            {
                number = _value2[0] * Mathf.Pow(10, digitdis) + _value1[0];
                reValue = Currencying(number);
                reValue[1] = reValue[1] + _value1[1];               
            }
            else
            {                
                number = _value1[0] * Mathf.Pow(10, digitdis) + _value2[0];
                reValue = Currencying(number);
                reValue[1] = reValue[1] + _value2[1];
            }
            return reValue;
        }
    }

    public float[] Minus(float[] _value1, float[] _value2)//Floating 된 두 수 빼기
    {
        float[] reValue;

        int digitdis = (int)Mathf.Abs(_value1[1] - _value2[1]);

        if (Mathf.Abs(_value1[1] - _value2[1]) > 5)
        {
            if (_value1[1] - _value2[1] < 0)
            {
                return _value2;
            }
            else
            {
                return _value1;
            }
        }
        else
        {
            float number;
            if (_value1[1] - _value2[1] < 0)
            {
                
                number = _value2[0] * Mathf.Pow(10, digitdis) - _value1[0];
                reValue = Currencying(number);
                reValue[1] = reValue[1] + _value1[1];
            }
            else
            {
                number = _value1[0] * Mathf.Pow(10, digitdis) - _value2[0];
                reValue = Currencying(number);
                reValue[1] = reValue[1] + _value2[1];
            }
            return reValue;
        }
    }

    public float[] Multiple(float[] _value1, float[] _value2)//Floating된 두 수 곱하기
    {
        float[] reValue;

        float value = _value1[0] * _value2[0];
        int digit = (int)(_value1[1] + _value2[1]);

        reValue = Currencying(value);
        reValue[1] = reValue[1] + digit;
        

        return reValue;
    }

    public float[] Multiple(float[] _value1, float _value2)
    {
        float[] array;
        array = Currencying(_value2);
        float[] reValue;
        reValue = Multiple(_value1, array);

        return reValue;
    }

    public float[] power(float[] _value, int _exponent)
    {
        
        int digiter = 0;
        float number = _value[0];

        for (int i = 0; i < _exponent; i++)
        {
            number = number * _value[0];
            int s_digit = Mathf.FloorToInt(Mathf.Log10(number));
            number = number * Mathf.Pow(0.1f, s_digit);
            number = (float)System.Math.Round(number, 1);
            digiter += s_digit;            
        }

        float[] reValue = new float[2];

        reValue[0] = number;
        reValue[1] = _value[1] + digiter;

        return reValue;
    }

    public int CompareFloat(float[] _value1, float[] _value2)//앞이 더 큼 : 0 // 뒤가 더 큼 : 1 // 같음 : 2
    {        
        if (_value1[1] > _value2[1])
        {
            return 0;
        }
        else if(_value2[1] > _value1[1])
        {
            return 1;
        }
        else
        {
            if(_value1[0] > _value2[0])
            {
                return 0;
            }
            else if(_value2[0] > _value1[0])
            {
                return 1;
            }
            else
            {
                return 2;
            }
        }
    }

}
