using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class RelicSlot : MonoBehaviour
{
    public Button UnActive;
    public Button Evolve, UnEvolve;
    public TextMeshProUGUI Tm_Name, Tm_Lv, Tm_Description, Tm_Cost, Tm_EarnedCount;
    public GameObject go_Evolve;
    public Text name_Ko;
    public bool watingEvolve;
    
    public void FillTexts(string _name, string _lv, string _description, string _value, string _cost, string _earnedCount)
    {
        Tm_Name.text = _name;
        Tm_Lv.text = "LV." + _lv;
        Tm_Description.text = _description + _value + "%";
        Tm_Cost.text = _cost;
        Tm_EarnedCount.text = _earnedCount;
    }

    public void LevelupText(string _lv, string _description, string _value)
    {
        Tm_Lv.text = "LV." + _lv;
        Tm_Description.text = _description + _value + "%";
    }

    public void CountUp(string _earnedCount) 
    {
        Tm_EarnedCount.text = _earnedCount;
    }

}
