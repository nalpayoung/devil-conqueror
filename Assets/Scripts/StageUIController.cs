using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class StageUIController : MonoBehaviour
{
    //DB에서 받아올 데이터
    int currStage = 0;
    float stageHP = 0;
    float currHP = 0;

    public GameObject Hpslider;
    public Image[] HpSliderImgs;
    public TextMeshProUGUI HpText;
    DataBaseManager theData;
    WaitForSeconds wait = new WaitForSeconds(0.005f);
    WaitForSeconds hpwait = new WaitForSeconds(0.05f);

    void Start()
    {
        theData = FindObjectOfType<DataBaseManager>();
        //Test
        stageHP = 234;
        currHP = stageHP;
    }

    public void StageUI_Initializing()
    {
        currStage = theData.currStage;
        stageHP = (float)theData.StageData[currStage]["stageHp"];
        currHP = stageHP;
        HpText.text = stageHP.ToString();
        Hpslider.GetComponent<Slider>().value = 1;
    }

    public void FadeOutHpSlider()
    {
        StopAllCoroutines();
        StartCoroutine(FadeOutHpSliderCoroutine());
    }

    IEnumerator FadeOutHpSliderCoroutine()
    {
        float _a = 1;

        for (int i = 0; i < HpSliderImgs.Length; i++)
        {
            Color color = HpSliderImgs[i].color;
            color.a = 1;
            HpSliderImgs[i].color = color;
        }

        Color txt_color = HpText.color;
        txt_color.a = 1;
        HpText.color = txt_color;

        while (_a > 0.01f)
        {
            for (int i = 0; i < HpSliderImgs.Length; i++)
            {
                Color color = HpSliderImgs[i].color;
                color.a = _a;
                HpSliderImgs[i].color = color;
                yield return wait;
            }

            Color txtm_color = HpText.color;
            txtm_color.a = _a;
            HpText.color = txtm_color;

            _a -= 0.05f;
        }
    }

    public void FadeInHpSlider()
    {
        StopAllCoroutines();
        StartCoroutine(FadeInHpSliderCoroutine());
    }

    IEnumerator FadeInHpSliderCoroutine()
    {
        float _a = 0;

        for (int i = 0; i < HpSliderImgs.Length; i++)
        {
            Color color = HpSliderImgs[i].color;
            color.a = 0;
            HpSliderImgs[i].color = color;
        }

        Color txt_color = HpText.color;
        txt_color.a = 0;
        HpText.color = txt_color;

        while (_a < 0.99f)
        {
            for (int i = 0; i < HpSliderImgs.Length; i++)
            {
                Color color = HpSliderImgs[i].color;
                color.a = _a;
                HpSliderImgs[i].color = color;
                yield return wait;
            }

            Color txtm_color = HpText.color;
            txtm_color.a = _a;
            HpText.color = txtm_color;

            _a += 0.05f;
        }
    }

    public void Slider_HpMinus(float _minushp)
    {
        StartCoroutine(Slider_HpMinusCoroutine(_minushp));
    }

    IEnumerator Slider_HpMinusCoroutine(float _minushp)
    {
        for (int i = 0; i < 10; i++)
        {
            currHP -= _minushp * 0.1f;
            Hpslider.GetComponent<Slider>().value = currHP / stageHP;
            yield return hpwait;
        }
    }


}