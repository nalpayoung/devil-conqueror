using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SliderController : MonoBehaviour
{
    public static SliderController instance = null;

    DataBaseManager database;   
    MonsterProduct theProduct;
    UserDataController theUser;

    public GameObject normalContent, EliteContent;
    public float[] normalCoolTime = new float[30];
    float normalCoolTimeMinus = 0;
    public float[] EliteCooltime = new float[30];
    public float[] EliteDuration = new float[30];
    float eliteCoolTimeMinus = 0;
    float eliteDurationAdd = 0;

    WaitForSeconds time = new WaitForSeconds(0.025f);
    float timeFloat = 0.025f;

    void Start()
    {
        database = FindObjectOfType<DataBaseManager>();        
        theProduct = FindObjectOfType <MonsterProduct>();
        theUser = FindObjectOfType<UserDataController>();

        instance = this;
    }

    public void Initialize()
    {
        StartCoroutine(InitializeCoroutine());
    }

    IEnumerator InitializeCoroutine()
    {
        yield return StartCoroutine(SetCoolTime());
        yield return StartCoroutine(StartMakeMonsters());
    }

    IEnumerator SetCoolTime()
    {
        normalCoolTimeMinus = theUser.n_monsterCoolTimeMinus;
        eliteCoolTimeMinus = theUser.e_eliteCoolTimeMinus;
        eliteDurationAdd = theUser.e_eliteDrationAdd;

        for (int i = 0; i < EliteCooltime.Length; i++)
        {
            object obj = database.EliteMonsterData[i]["eliteMonsterCoolTime"];
            if (obj.GetType().Name == "Int32")
            {
                EliteCooltime[i] = (int)database.EliteMonsterData[i]["eliteMonsterCoolTime"] * (1 - eliteCoolTimeMinus);
            }
            else
            {
                EliteCooltime[i] = (float)database.EliteMonsterData[i]["eliteMonsterCoolTime"] * (1 - eliteCoolTimeMinus);
            }

            obj = database.EliteMonsterData[i]["eliteMonsterDuration"];
            if (obj.GetType().Name == "Int32")
            {
                EliteDuration[i] = (int)database.EliteMonsterData[i]["eliteMonsterDuration"] * (1 + eliteDurationAdd);
            }
            else
            {
                EliteDuration[i] = (float)database.EliteMonsterData[i]["eliteMonsterDuration"] * (1 + eliteDurationAdd);
            }
        }

        for (int i = 0; i < normalCoolTime.Length; i++)
        {
            object obj_n = database.NormalMonsterData[i]["norMonsterCoolTime"];

            if (obj_n.GetType().Name == "Int32")
            {
                normalCoolTime[i] = (int)database.NormalMonsterData[i]["norMonsterCoolTime"] * (1 - normalCoolTimeMinus);
            }
            else
            {
                normalCoolTime[i] = (float)database.NormalMonsterData[i]["norMonsterCoolTime"] * (1 - normalCoolTimeMinus);
            }
        }
        yield return null;

        Debug.Log("쿨타임 세팅 완료");
    }

    IEnumerator StartMakeMonsters()
    {
        Debug.Log("몬스터만들기");
        for (int i = 0; i < theUser.monsterSlotActiveNumber + 1; i++)
        {
            MakeNormalMonsterChecker(i);
        }

        for (int i = 0; i < theUser.eliteSlotActiveNumber + 1; i++)
        {
            MakeEliteMonsterChecker(i);
        }
        yield return null;
    }

    public void MakeNormalMonsterChecker(int _slotIndex)
    {
        StartCoroutine(MakeNormalMonsterCoolTimeCheckerCoroutine(_slotIndex));
    }

    IEnumerator MakeNormalMonsterCoolTimeCheckerCoroutine(int _slotindex)
    {        
        float currTime = 0;
        
        float coolTime = normalCoolTime[_slotindex];
        if (coolTime != 0)
        {
            while (currTime < coolTime)
            {
                currTime += timeFloat;
                if (normalContent.activeSelf)
                {
                    normalContent.transform.GetChild(_slotindex).GetComponent<MonsterSlot>().slider.value = currTime / normalCoolTime[_slotindex];
                }
                yield return time;
            }
            theProduct.DeQueueMonster(_slotindex);
            MakeNormalMonsterChecker(_slotindex);
        }
            
    }

    public void MakeEliteMonsterChecker(int _slotindex)
    {
        StartCoroutine(MakeEliteMonsterCoolTimeCheckerCoroutine(_slotindex));
    }

    IEnumerator MakeEliteMonsterCoolTimeCheckerCoroutine(int _slotindex)
    {
        float currTime = 0;
        float coolTime = EliteCooltime[_slotindex];
        if (coolTime != 0)
        {
            while (currTime < coolTime)
            {
                currTime += timeFloat;
                if (EliteContent.activeSelf)
                {
                    EliteContent.transform.GetChild(_slotindex).GetComponent<EliteMonsterSlot>().slider.value = currTime / EliteCooltime[_slotindex];
                }
                yield return time;
            }
            theProduct.DeQueueElite(_slotindex);
            MakeEliteMonsterChecker(_slotindex);

        }
    }
}
