using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class UpperUI : MonoBehaviour
{
    public TextMeshProUGUI txt_Gem, txt_Gold;
    public Button Btn_Setting;
    public GameObject Popup_Setting;
    public Slider slider;
    CurrencyManager thecurrency;
    UserDataController theUser;

    void Start()
    {
        theUser = FindObjectOfType<UserDataController>();
        thecurrency = FindObjectOfType<CurrencyManager>();
    }

    public void Initialize()
    {
        txt_Gem.text = theUser.currGem.ToString();
        txt_Gold.text = thecurrency.NumberToString(theUser.currGold);
        slider.value = 1;
    }


    public void OpenSetting()
    {
        Popup_Setting.SetActive(true);
    }

    public void GoldApply()
    {
        txt_Gold.text = thecurrency.NumberToString(theUser.currGold);
    }
}