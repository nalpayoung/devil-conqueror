using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine.SceneManagement;

public class SaveAndLoad : MonoBehaviour
{
    [System.Serializable]
    public class Data
    {
        public float[] currgold;
        public int gem;
        public int masterlevel;

        public int normalSlotActivate;
        public List<int> eliteslotActivate;
        public List<int> normalMonsterLevel;
    }

    DataBaseManager theData;
    SlotController theSlot;
    public Data data;

    public void CallSave()
    {
        
    }

    public void CallLoad()
    {
    }
}
