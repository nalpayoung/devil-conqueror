using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class DataBaseManager : MonoBehaviour
{
        //세이브데이터
        public bool isSaveData = false;//첫세이브냐 아니냐. 첫 데이터로 설정된 걸 쓰느냐 안쓰느냐

        //재화 데이터
        public float[] currGold = new float[2];
        public float[] currStone = new float[2];//장비강화석        
        public int currGem = 0;
        public int currBadge = 0;//엘리트강화석
        public float[] relicShards = new float[2];//유물강화재료
        public float[] orb = new float[2]; //마력강화재료
        public float maxMp = 0;

        //몬스터 슬롯 정보
        public int[] a_normalMonsterSlotLv = new int[30];
        public int[] a_eliteMonsterSlotLv = new int[30];
        public int monsterSlotActiveNumber = 0;
        public int eliteSlotActiveNumber = 0;

        //장비 정보
        public bool[] armorActivate = new bool[24];
        public bool[] weaponActivate = new bool[24];
        public int[] armorCount = new int[24];//개수
        public int[] weaponCount = new int[24];
        public int[] armorLv = new int[24];
        public int[] weaponLv = new int[24];
        public int equpipedArmorNo;
        public int equipedWeaponNo;

        //유물정보
        public int[] relicLv = new int[13];
        public bool[] relicActiveList = new bool[13];
        public int[] relicEarnedCount = new int[13];
        public int relicSummonCount;

        //스킬 정보
        public int[] skillLv = new int[5];

        //마력 정보
        public int evilPowerLv = 0;

        //스테이지 정보
        public int currStage;

        //타이틀 정보
        public int currTitleNo = 0;



    //데미지, 가격등 스트링으로 바꿔주는 컴포넌트
    private CurrencyManager theCurr;
    
    
    //csv 받아오기
    public List<Dictionary<string, object>> WeaponData = new List<Dictionary<string, object>>();
    public List<Dictionary<string, object>> WeaponEquipedAtkAddData = new List<Dictionary<string, object>>();
    public List<Dictionary<string, object>> WeaponUnequipedAtkAddData = new List<Dictionary<string, object>>();
    public List<Dictionary<string, object>> ArmorData = new List<Dictionary<string, object>>();
    public List<Dictionary<string, object>> ArmorEquipedHpAddData = new List<Dictionary<string, object>>();
    public List<Dictionary<string, object>> ArmorUnequipedHpAddData = new List<Dictionary<string, object>>();
    public List<Dictionary<string, object>> equipPriceData = new List<Dictionary<string, object>>();
    public List<Dictionary<string, object>> EliteMonsterData = new List<Dictionary<string, object>>();
    public List<Dictionary<string, object>> NormalMonsterData = new List<Dictionary<string, object>>();
    public List<Dictionary<string, object>> RelicData = new List<Dictionary<string, object>>();    
    public List<Dictionary<string, object>> SkillData = new List<Dictionary<string, object>>();
    public List<Dictionary<string, object>> StageData = new List<Dictionary<string, object>>();
    public List<Dictionary<string, object>> TitleData = new List<Dictionary<string, object>>();

    Dictionary<int, int> ArtifactNeedCard200 = new Dictionary<int, int>();
    Dictionary<int, int> ArtifactNeedCard500 = new Dictionary<int, int>();


    void Awake()
    {
        theCurr = FindObjectOfType<CurrencyManager>();
        FillArtifactNeedCard();
        //나중에 명령 컨트롤러에서 모두 순서 정해서 하기
        //1. 초기값 채우기, 2,세이브데이터 반영, 3.슬롯들 생성, 4.Active 상태 체크
        //csv 데이터 채우기        
        //isSaveData가 true면 LoadData()를 돌리고 False면 첫플레이이므로 SetInitialData()를 돌림


    }

    void FillArtifactNeedCard()//유물 카드 겹치기
    {
        ArtifactNeedCard200.Add(25, 2);
        ArtifactNeedCard200.Add(50, 5);
        ArtifactNeedCard200.Add(75, 10);
        ArtifactNeedCard200.Add(100, 15);
        ArtifactNeedCard200.Add(125, 20);
        ArtifactNeedCard200.Add(150, 25);
        ArtifactNeedCard200.Add(175, 30);
        ArtifactNeedCard200.Add(200, 35);

        ArtifactNeedCard500.Add(50, 2);
        ArtifactNeedCard500.Add(100, 5);
        ArtifactNeedCard500.Add(150, 8);
        ArtifactNeedCard500.Add(200, 10);
        ArtifactNeedCard500.Add(250, 13);
        ArtifactNeedCard500.Add(300, 15);
        ArtifactNeedCard500.Add(350, 23);
        ArtifactNeedCard500.Add(400, 28);
        ArtifactNeedCard500.Add(450, 32);
        ArtifactNeedCard500.Add(500, 35);
    }

    public void SetTestData()
    {
        currGold[0] = 5;
        currGold[1] = 1;
        currStone[0] = 0;
        currStone[0] = 0;
        currGem = 100;
        currBadge = 0;
        relicShards[0] = 0;
        relicShards[1] = 0;
        orb[0] = 0;
        orb[1] = 0;
        maxMp = 50;

        for (int i = 0; i < a_normalMonsterSlotLv.Length; i++)
        {
            a_normalMonsterSlotLv[i] = 0;
            a_eliteMonsterSlotLv[i] = 0;
        }
        monsterSlotActiveNumber = 0;
        eliteSlotActiveNumber = -1;

        equipedWeaponNo = 0;
        equipedWeaponNo = 0;

        for (int i = 0; i < armorActivate.Length; i++)
        {
            armorActivate[i] = false;
            weaponActivate[i] = false;
        }

        //Test
        for (int i = 0; i < 3; i++)
        {
            armorActivate[i] = true;
            weaponActivate[i] = true;
        }


        for (int i = 0; i < armorCount.Length; i++)
        {
            armorCount[i] = 0;
            weaponCount[i] = 0;
            armorLv[i] = 0;
            weaponLv[i] = 0;
        }

        //Test
        for (int i = 0; i < 3; i++)
        {
            armorCount[i] = 6;
            weaponCount[i] = 6;
            armorLv[i] = 3;
            weaponLv[i] = 3;
        }


        for (int i = 0; i < relicLv.Length; i++)
        {
            relicLv[i] = 0;
            relicActiveList[i] = false;
            relicEarnedCount[i] = 0;
        }        

        //Test_Artifact
        for (int i = 0; i < 3; i++)
        {
            relicActiveList[i] = true;
        }
        relicSummonCount = 3;

        for (int i = 0; i < skillLv.Length; i++)
        {
            skillLv[i] = 0;
        }

        evilPowerLv = 0;
        currStage = 0;
        currTitleNo = 0;
    }

    public void SetInitialData()//세이브 데이터가 없을 경우 이 데이터를 덮어씀
    {
        currGold[0] = 5;
        currGold[1] = 1;
        currStone[0] = 0;
        currStone[0] = 0;
        currGem = 100;
        currBadge = 0;
        relicShards[0] = 0;
        relicShards[1] = 0;
        orb[0] = 0;
        orb[1] = 0;
        maxMp = 50;

        for (int i = 0; i < a_normalMonsterSlotLv.Length; i++)
        {
            a_normalMonsterSlotLv[i] = 0;
            a_eliteMonsterSlotLv[i] = 0;
        }
        monsterSlotActiveNumber = 0;
        eliteSlotActiveNumber = -1;

        equipedWeaponNo = 0;
        equipedWeaponNo = 0;

        for (int i = 0; i < armorActivate.Length; i++)
        {
            armorActivate[i] = false;
            weaponActivate[i] = false;
        }


        for (int i = 0; i < armorCount.Length; i++)
        {            
            armorCount[i] = 0;
            weaponCount[i] = 0;
            armorLv[i] = 0;
            weaponLv[i] = 0;
        }
        

        for (int i = 0; i < relicLv.Length; i++)
        {
            relicLv[i] = 0;
            relicActiveList[i] = false;
            relicEarnedCount[i] = 0;
        }

        relicSummonCount = 0;

        for (int i = 0; i < skillLv.Length; i++)
        {
            skillLv[i] = 0;
        }

        evilPowerLv = 0;
        currStage = 0;
        currTitleNo = 0;
    }

    public void LoadCsv()
    {
        WeaponData = CSVReader.Read("csv/weapon");
        WeaponEquipedAtkAddData = CSVReader.Read("csv/weaponEquipedAtkAdd");        
        WeaponUnequipedAtkAddData = CSVReader.Read("csv/weaponUnequipedAtkAdd");
        EliteMonsterData = CSVReader.Read("csv/eliteMonster");
        NormalMonsterData = CSVReader.Read("csv/normalMonster");
        ArmorData = CSVReader.Read("csv/armor");
        RelicData = CSVReader.Read("csv/relic");        
        SkillData = CSVReader.Read("csv/skill");
        StageData = CSVReader.Read("csv/stage");
        TitleData = CSVReader.Read("csv/title");
        equipPriceData = CSVReader.Read("csv/equipmentPrice");
        ArmorEquipedHpAddData = CSVReader.Read("csv/armorEquipedHpAdd");
        ArmorUnequipedHpAddData = CSVReader.Read("csv/armorUnequipedHpAdd");       
    }
    
    public void SaveData()
    {

    }

    public void LoadData()
    {

    }
}
